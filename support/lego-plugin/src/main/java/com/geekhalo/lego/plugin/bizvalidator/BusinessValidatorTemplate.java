package com.geekhalo.lego.plugin.bizvalidator;

import com.geekhalo.lego.plugin.support.template.CreateClassContext;
import com.geekhalo.lego.plugin.support.util.Utils;

public class BusinessValidatorTemplate {
    private static final String TEMPLATE =
            "package {pkg};\n" +
            "\n" +
            "import com.geekhalo.lego.common.validator.ValidateErrorHandler;\n" +
            "import com.geekhalo.lego.core.validator.AbstractBusinessValidator;\n" +
            "import {cxtTypeFull};\n" +
            "import lombok.extern.slf4j.Slf4j;\n" +
            "import org.springframework.stereotype.Component;\n" +
            "\n" +
            "@Component\n" +
            "@Slf4j\n" +
            "public class {className}\n" +
            "    extends AbstractBusinessValidator<{cxtType}> {\n" +
            "\n" +
            "    public {className}(){\n" +
            "        super({cxtType}.class);\n" +
            "    }\n" +
            "\n" +
            "    @Override\n" +
            "    public void validate({cxtType} context, ValidateErrorHandler validateErrorHandler) {\n" +
            "        log.info(\"validate(context) {}\", context);\n" +
            "    }\n" +
            "}\n";
    public static String create(CreateBusinessValidatorContext context){
        String content = TEMPLATE;
        content = content.replace("{pkg}", context.getPkg());
        content = content.replace("{className}", context.getClsName());
        content = content.replace("{cxtTypeFull}", context.getCxtTypeFull());
        content = content.replace("{cxtType}", context.getCxtType());


        return content;
    }

    public static class CreateBusinessValidatorContext extends CreateClassContext {
        private String cxtTypeFull;
        private String cxtType;
        public CreateBusinessValidatorContext(String pkg, String clsName) {
            super(pkg, clsName);
        }

        public void setCxtTypeFull(String cxtTypeFull) {
            this.cxtTypeFull = cxtTypeFull;
            this.cxtType = Utils.getType(cxtTypeFull);
        }
        public String getCxtTypeFull() {
            return cxtTypeFull;
        }

        public String getCxtType() {
            return cxtType;
        }
    }
}
