package com.geekhalo.lego.plugin.bizvalidator;

import com.geekhalo.lego.plugin.support.action.BaseAnAction;
import com.geekhalo.lego.plugin.creator.JavaFileCreator;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.vfs.VirtualFile;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

public class CreateBusinessValidatorAction extends BaseAnAction {
    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        String pkg = getPackage(e);
        if (StringUtils.isEmpty(pkg)){
            Messages.showWarningDialog("选择正确的包名", "选择包");
            return;
        }

        VirtualFile[] filesInPackage = getFilesInPackage(e);
        String cxtType = findFullType(filesInPackage, name -> name.endsWith("Context"));
        if (StringUtils.isEmpty(cxtType)) {
            Messages.showWarningDialog("请选择具体的 Context", "Warning");
            return;
        }

        String clsName = Messages.showInputDialog(e.getProject(), "输入类名", "类名", null);
        if (StringUtils.isEmpty(clsName)) {
            Messages.showWarningDialog("请输入类名", "Warning");
            return;
        }

        Module currentModule = findCurrentModule(e);

        BusinessValidatorTemplate.CreateBusinessValidatorContext context = new BusinessValidatorTemplate
                .CreateBusinessValidatorContext(pkg,  clsName);
        context.setCxtTypeFull(cxtType);

        String content = BusinessValidatorTemplate.create(context);

        JavaFileCreator.createJavaFileInPackage(e.getProject(), currentModule, pkg, clsName, content);
    }
}
