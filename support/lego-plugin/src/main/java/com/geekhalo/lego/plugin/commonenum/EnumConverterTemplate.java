package com.geekhalo.lego.plugin.commonenum;

import com.geekhalo.lego.plugin.support.template.CreateClassContext;

public class EnumConverterTemplate {
    private static final String JPA_TEMPLATE =
            "package {pkg};\n" +
                    "\n" +
                    "import com.geekhalo.lego.core.enums.repository.jpa.CommonEnumAttributeConverter;\n" +
                    "import javax.persistence.Converter;\n" +
                    "import {enumTypeFull};\n" +

                    "\n" +
                    "@Converter(autoApply = true)\n" +
                    "public class {className} \n" +
                    "       extends CommonEnumAttributeConverter<{enumType}> { \n" +
                    "\n" +
                    "\n" +
                    "   public {className}(){ \n" +
                    "       super({enumType}.values());\n" +
                    "   }\n" +
                    "\n" +
                    "\n}";
    private static final String MY_BATIS_TEMPLATE =
            "package {pkg};\n" +
                    "\n" +
                    "import com.geekhalo.lego.core.enums.repository.mybatis.CommonEnumTypeHandler;\n" +
                    "import org.apache.ibatis.type.MappedTypes;\n"+
                    "import {enumTypeFull};\n" +
                    "\n" +
                    "@MappedTypes({enumType}.class)\n" +
                    "public class {className} \n" +
                    "       extends CommonEnumTypeHandler<{enumType}> { \n" +
                    "\n" +
                    "\n" +
                    "   public {className}(){ \n" +
                    "       super({enumType}.values());\n" +
                    "   }\n" +
                    "\n" +
                    "\n}";


    public static String createMyBatisConverter(CreateConverterContext context) {
        String content = MY_BATIS_TEMPLATE;
        content = content.replace("{pkg}", context.getPkg());
        content = content.replace("{className}", context.getClsName());
        content = content.replace("{enumTypeFull}", context.getEnumTypeFull());
        content = content.replace("{enumType}", context.getEnumType());
        return content;
    }

    public static String createJpaConverter(CreateConverterContext context) {
        String content = JPA_TEMPLATE;
        content = content.replace("{pkg}", context.getPkg());
        content = content.replace("{className}", context.getClsName());
        content = content.replace("{enumTypeFull}", context.getEnumTypeFull());
        content = content.replace("{enumType}", context.getEnumType());
        return content;
    }

    public static class CreateConverterContext extends CreateClassContext {
        private String enumTypeFull;
        private String enumType;
        public CreateConverterContext(String pkg, String clsName) {
            super(pkg, clsName);
        }

        public void setEnumType(String pkg, String enumType) {
            this.enumTypeFull = pkg + "." + enumType;
            this.enumType = enumType;
        }

        public String getEnumType() {
            return enumType;
        }

        public String getEnumTypeFull() {
            return enumTypeFull;
        }
    }
}
