package com.geekhalo.lego.plugin.aggfactory;

import com.geekhalo.lego.plugin.support.template.CreateClassContext;
import com.geekhalo.lego.plugin.support.util.Utils;

public class AggFactoryTemplate {
    private static final String TEMPLATE =
            "package {pkg};\n" +
                    "\n" +
                    "import com.geekhalo.lego.core.command.support.handler.aggfactory.AbstractSmartAggFactory;\n" +
                    "import org.springframework.stereotype.Component;\n" +
                    "import lombok.extern.slf4j.Slf4j;\n" +
                    "import {contextTypeFull};\n" +
                    "import {aggTypeFull};\n" +
                    "\n" +
                    "@Component\n" +
                    "@Slf4j\n" +
                    "public class {className}\n" +
                    "        extends AbstractSmartAggFactory<{contextType}, {aggType}> {\n" +
                    "\n" +
                    "    public {className}(){\n" +
                    "        super({contextType}.class, {aggType}.class);\n" +
                    "    }\n" +
                    "\n" +
                    "    @Override\n" +
                    "    public {aggType} create({contextType} context) {\n" +
                    "        return {aggType}.create(context);\n" +
                    "    }\n" +
                    "}";

    public static String create(CreateAggFactoryContext context){
        String content = TEMPLATE;
        content = content.replace("{pkg}", context.getPkg());
        content = content.replace("{className}", context.getClsName());
        content = content.replace("{aggType}", context.getAggType());
        content = content.replace("{aggTypeFull}", context.getAggTypeFull());

        content = content.replace("{contextType}", context.getContextType());
        content = content.replace("{contextTypeFull}", context.getContextTypeFull());

        return content;
    }
    public static class CreateAggFactoryContext extends CreateClassContext {
        private String aggType;
        private String aggTypeFull;
        private String contextType;
        private String contextTypeFull;

        public CreateAggFactoryContext(String pkg, String clsName) {
            super(pkg, clsName);
        }

        public void setAggTypeFull(String aggTypeFull){
            this.aggTypeFull = aggTypeFull;
            this.aggType = Utils.getType(aggTypeFull);
        }

        public void setContextTypeFull(String contextTypeFull){
            this.contextTypeFull = contextTypeFull;
            this.contextType = Utils.getType(contextTypeFull);
        }

        @Override
        public String getAggType() {
            return aggType;
        }

        @Override
        public String getAggTypeFull() {
            return aggTypeFull;
        }

        public String getContextType() {
            return contextType;
        }

        public String getContextTypeFull() {
            return contextTypeFull;
        }
    }
}
