package com.geekhalo.lego.plugin.aggregation;

import com.geekhalo.lego.plugin.support.template.CreateClassContext;
import com.geekhalo.lego.plugin.support.util.Utils;

public class RepositoryTemplate {
    private static final String COMMAND_TEMPLATE =
            "package {pkg};\n" +
            "\n" +
            "\n" +
            "import com.geekhalo.lego.core.command.CommandRepository;\n" +
            "import {aggTypeFull};\n" +
            "import {idTypeFull};\n" +
            "\n" +
            "import java.util.Optional;\n" +
            "\n" +
            "public interface {className} extends CommandRepository<{aggType}, {idType}> {\n" +
            "\n" +
            "\n" +
            "\n" +
            "}\n";
    private static final String QUERY_TEMPLATE =
            "package {pkg};\n" +
            "\n" +
            "\n" +
            "import com.geekhalo.lego.core.query.QueryRepository;\n" +
            "import {viewTypeFull};\n" +
            "import {viewIdTypeFull};\n" +
            "\n" +
            "import java.util.Optional;\n" +
            "\n" +
            "public interface {className} extends QueryRepository<{viewType}, {viewIdType}> {\n" +
            "\n" +
            "\n" +
            "\n" +
            "}\n";

    public static String createCommand(CreateCommandRepositoryContext context) {
        String content = COMMAND_TEMPLATE;
        content = content.replace("{pkg}", context.getPkg());
        content = content.replace("{className}", context.getClsName());
        content = content.replace("{aggType}", context.getAggType());
        content = content.replace("{aggTypeFull}", context.getAggTypeFull());
        content = content.replace("{idType}", context.getIdType());
        content = content.replace("{idTypeFull}", context.getIdTypeFull());
        return content;
    }

    public static String createQuery(CreateQueryRepositoryContext context) {
        String content = QUERY_TEMPLATE;
        content = content.replace("{pkg}", context.getPkg());
        content = content.replace("{className}", context.getClsName());
//        content = content.replace("{aggType}", context.getAggType());
//        content = content.replace("{aggTypeFull}", context.getAggTypeFull());
//        content = content.replace("{idType}", context.getIdType());
//        content = content.replace("{idTypeFull}", context.getIdTypeFull());

        content = content.replace("{viewType}", context.getViewType());
        content = content.replace("{viewTypeFull}", context.getViewTypeFull());
        content = content.replace("{viewIdType}", context.getViewIdType());
        content = content.replace("{viewIdTypeFull}", context.getViewIdTypeFull());
        return content;
    }

    public static class CreateCommandRepositoryContext extends CreateClassContext {
        public CreateCommandRepositoryContext(String pkg, String clsName) {
            super(pkg, clsName);
        }
    }

    public static class CreateQueryRepositoryContext extends CreateClassContext{
        private String viewType;
        private String viewTypeFull;
        private String viewIdType;
        private String viewIdTypeFull;

        public void setViewTypeFull(String viewTypeFull){
            this.viewTypeFull = viewTypeFull;
            this.viewType = Utils.getType(viewTypeFull);
        }

        public void setViewIdTypeFull(String viewIdTypeFull){
            this.viewIdTypeFull = viewIdTypeFull;
            this.viewIdType = Utils.getType(viewIdTypeFull);
        }

        public String getViewType() {
            return viewType;
        }

        public String getViewTypeFull() {
            return viewTypeFull;
        }

        public String getViewIdType() {
            return viewIdType;
        }

        public String getViewIdTypeFull() {
            return viewIdTypeFull;
        }

        public CreateQueryRepositoryContext(String pkg, String clsName) {
            super(pkg, clsName);
        }
    }
}
