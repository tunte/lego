package com.geekhalo.lego.plugin.support.ui;

import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiParameter;
import com.intellij.psi.PsiParameterList;

public class TableValue {
    private final PsiClass psiClass;
    private final PsiMethod method;

    public TableValue(PsiClass psiClass, PsiMethod method) {
        this.psiClass = psiClass;
        this.method = method;
    }

    public PsiClass getPsiClass() {
        return psiClass;
    }

    public PsiMethod getMethod() {
        return method;
    }

    @Override
    public String toString() {
        String returnType =  method.getReturnType().getCanonicalText();
        String methodName = method.getName();
        StringBuilder parameterStr = new StringBuilder();
        parameterStr.append("(");
        PsiParameterList parameterList = method.getParameterList();

        for (PsiParameter parameter: parameterList.getParameters()){
            parameterStr.append(parameter.getType().getCanonicalText());
            parameterStr.append(" ");
            parameterStr.append(parameter.getName());
            parameterStr.append(",");
        }
        if (!parameterList.isEmpty()){
            parameterStr.deleteCharAt(parameterStr.length() - 1);
        }
        parameterStr.append(")");
        return returnType + " " + methodName + parameterStr;
    }
}