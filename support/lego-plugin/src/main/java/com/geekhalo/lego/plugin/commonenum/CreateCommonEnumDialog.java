package com.geekhalo.lego.plugin.commonenum;

import com.geekhalo.lego.plugin.creator.JavaFileCreator;
import com.geekhalo.lego.plugin.support.ui.DocumentUpdateListener;
import com.geekhalo.lego.plugin.support.util.Utils;
import com.intellij.ide.util.PackageChooserDialog;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.Messages;
import com.intellij.psi.PsiPackage;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class CreateCommonEnumDialog extends DialogWrapper {
    private JPanel contentPane;
    private JTextField enumClassName;
    private JCheckBox jpaConverterEnable;
    private JCheckBox myBatisEnable;
    private JTextField moduleName;
    private JTextField pkgName;
    private JButton selectPkg;
    private JTextField jpaConverterName;
    private JTextField myBatisHandlerName;

    private final Project project;
    private final Module domainModule;
    private final Module infraModule;
    private final String pkg;

    public CreateCommonEnumDialog(Project project,
                                  Module domainModule,
                                  Module infraModule,
                                  String pkg) {
        super(project);
        setModal(true);
        this.project = project;
        this.domainModule = domainModule;
        this.infraModule = infraModule;
        this.pkg = pkg;
        initComponent();
        init();
    }

    private void initComponent() {
        this.moduleName.setText(this.infraModule.getName());
        this.pkgName.setText(Utils.domainPkgToInfra(this.pkg));
        // 枚举名称改变，Handle 也需要改变
        this.enumClassName.getDocument()
                .addDocumentListener(new DocumentUpdateListener(this::updateByEnum));

        // 选择包
        selectPkg.addActionListener(e -> {
            PackageChooserDialog chooserDialog = new PackageChooserDialog("请选择包", project);
            if(chooserDialog.showAndGet()) {
                PsiPackage selectedPackage = chooserDialog.getSelectedPackage();
                if (selectedPackage != null) {
                    pkgName.setText(selectedPackage.getQualifiedName());
                }
            }
        });
    }

    private void updateByEnum(String enumName) {
        this.jpaConverterName.setText(Utils.getEnumJpaConvertName(enumName));
        this.myBatisHandlerName.setText(Utils.getEnumMyBatisConvertName(enumName));
    }

    @Override
    protected void doOKAction(){
        createFiles();
        super.doOKAction();
    }

    private void createFiles(){
        if (!checkData()){
            return;
        }
        createEnum();
        createJpaConverter();
        createMyBatisHandler();
    }

    private void createMyBatisHandler() {
        if (this.myBatisEnable.isSelected()){
            String enumConverterName = this.myBatisHandlerName.getText();
            if (StringUtils.isEmpty(enumConverterName)){
                Messages.showWarningDialog("输入包名", "创建MyBatisHandler");
                return;
            }

            EnumConverterTemplate.CreateConverterContext context = new EnumConverterTemplate.CreateConverterContext(
                    this.pkgName.getText(),
                    enumConverterName
            );

            context.setEnumType(this.pkg, this.enumClassName.getText());
            String content = EnumConverterTemplate.createMyBatisConverter(context);
            JavaFileCreator.createJavaFileInPackage(this.project, infraModule, this.pkgName.getText(), enumConverterName, content);
        }
    }

    private void createJpaConverter() {
        if (this.jpaConverterEnable.isSelected()){
            String enumConverterName = this.jpaConverterName.getText();
            if (StringUtils.isEmpty(enumConverterName)){
                Messages.showWarningDialog("输入包名", "创建JpaConverter");
                return;
            }

            EnumConverterTemplate.CreateConverterContext context = new EnumConverterTemplate.CreateConverterContext(
                    this.pkgName.getText(),
                    enumConverterName
            );
            context.setEnumType(this.pkg, this.enumClassName.getText());
            String content = EnumConverterTemplate.createJpaConverter(context);
            JavaFileCreator.createJavaFileInPackage(this.project, infraModule, this.pkgName.getText(), enumConverterName, content);
        }
    }

    private void createEnum() {
        String enumName = this.enumClassName.getText();
        EnumTemplate.CreateEnumContext context = new EnumTemplate.CreateEnumContext(pkg, enumName);
        String content = EnumTemplate.create(context);
        JavaFileCreator.createJavaFileInPackage(this.project, domainModule, pkg, enumName, content);
    }

    private boolean checkData() {
        if (StringUtils.isEmpty(this.enumClassName.getText())){
            Messages.showWarningDialog("输入枚举名称", "创建枚举");
            return false;
        }
        return true;
    }

    @Override
    public void doCancelAction(){
        super.doCancelAction();
    }

    @Override
    protected @Nullable JComponent createCenterPanel() {
        return contentPane;
    }
}
