package com.geekhalo.lego.plugin.lazyloader;

import com.geekhalo.lego.plugin.support.action.BaseAnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.ui.Messages;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

public class CreateLazyLoaderAction extends BaseAnAction {
    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        String pkg = getPackage(e);
        if (StringUtils.isEmpty(pkg)){
            Messages.showWarningDialog("选择正确的包名", "选择包");
            return;
        }

        Module currentModule = findCurrentModule(e);
        CreateLazyLoaderDialog dialog = new CreateLazyLoaderDialog(e.getProject(), currentModule, pkg);
        dialog.show();
    }
}
