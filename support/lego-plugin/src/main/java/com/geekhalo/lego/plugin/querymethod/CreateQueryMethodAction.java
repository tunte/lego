package com.geekhalo.lego.plugin.querymethod;

import com.geekhalo.lego.plugin.support.action.BaseAnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.ui.Messages;
import com.intellij.psi.PsiClass;
import org.jetbrains.annotations.NotNull;

public class CreateQueryMethodAction extends BaseAnAction {


    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        PsiClass psiClass = getJavaClass(e);

        if (psiClass == null){
            Messages.showMessageDialog("请选择查询服务", "Warn", null);
            return;
        }
//        String pkg = Utils.getPkg(psiClass.getQualifiedName());
        Module currentModule = findCurrentModule(e);

        CreateQueryMethodDialog dialog = new CreateQueryMethodDialog(e.getProject(), currentModule, psiClass);
        dialog.show();
    }

}
