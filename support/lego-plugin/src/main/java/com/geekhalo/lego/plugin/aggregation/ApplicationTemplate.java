package com.geekhalo.lego.plugin.aggregation;

import com.geekhalo.lego.plugin.support.template.CreateClassContext;
import com.geekhalo.lego.plugin.support.util.Utils;

import static com.geekhalo.lego.plugin.support.util.Utils.getType;

public class ApplicationTemplate {
    private static final String COMMAND_TEMPLATE =
            "package {pkg};\n" +
            "\n" +
            "import com.geekhalo.lego.core.command.CommandServiceDefinition;\n" +
            "import org.springframework.validation.annotation.Validated;\n" +
            "import javax.validation.Valid;\n" +
            "import {commandRepositoryTypeFull};\n" +
            "import {aggTypeFull};\n" +
            "\n" +
            "@CommandServiceDefinition(\n" +
            "        domainClass = {aggType}.class,\n" +
            "        repositoryClass = {commandRepositoryType}.class\n" +
            ")\n" +
            "@Validated\n" +
            "public interface {className} {\n" +
            "\n" +
            "}\n";

    private static final String QUERY_TEMPLATE =
            "package {pkg};\n" +
            "\n" +
            "import com.geekhalo.lego.core.query.QueryServiceDefinition;\n" +
            "import org.springframework.validation.annotation.Validated;\n" +
            "import javax.validation.Valid;\n" +
            "import com.geekhalo.lego.core.singlequery.Page;\n" +
            "import java.util.List;\n" +
            "import {queryRepositoryTypeFull};\n" +
            "import {viewTypeFull};\n" +
            "\n" +
            "@QueryServiceDefinition(\n" +
            "        repositoryClass = {queryRepositoryType}.class,\n" +
            "        domainClass = {viewType}.class\n" +
            ")\n" +
            "@Validated\n" +
            "public interface {className} {\n" +
            "\n" +
            "}\n";
    public static String createCommandApplication(CreateCommandApplicationContext context){
        String content = COMMAND_TEMPLATE;
        content = content.replace("{pkg}", context.getPkg());
        content = content.replace("{className}", context.getClsName());
        content = content.replace("{aggType}", context.getAggType());
        content = content.replace("{aggTypeFull}", context.getAggTypeFull());
        content = content.replace("{commandRepositoryType}", context.getCommandRepositoryType());
        content = content.replace("{commandRepositoryTypeFull}", context.getCommandRepositoryTypeFull());

        return content;
    }

    public static class CreateCommandApplicationContext extends CreateClassContext {
        private String commandRepositoryType;
        private String commandRepositoryTypeFull;
        public CreateCommandApplicationContext(String pkg, String clsName) {
            super(pkg, clsName);
        }

        public void setCommandRepositoryTypeFull(String commandRepositoryTypeFull){
            this.commandRepositoryTypeFull = commandRepositoryTypeFull;
            this.commandRepositoryType = getType(commandRepositoryTypeFull);
        }

        public String getCommandRepositoryType() {
            return commandRepositoryType;
        }

        public String getCommandRepositoryTypeFull() {
            return commandRepositoryTypeFull;
        }
    }

    public static String createQueryApplication(CreateQueryApplicationContext context){
        String content = QUERY_TEMPLATE;
        content = content.replace("{pkg}", context.getPkg());
        content = content.replace("{className}", context.getClsName());
//        content = content.replace("{aggType}", context.getAggType());
//        content = content.replace("{aggTypeFull}", context.getAggTypeFull());
        content = content.replace("{queryRepositoryType}", context.getQueryRepositoryType());
        content = content.replace("{queryRepositoryTypeFull}", context.getQueryRepositoryTypeFull());

        content = content.replace("{viewType}", context.getViewType());
        content = content.replace("{viewTypeFull}", context.getViewTypeFull());

        return content;
    }

    public static class CreateQueryApplicationContext extends CreateClassContext{
        private String queryRepositoryType;
        private String queryRepositoryTypeFull;
        private String viewType;
        private String viewTypeFull;
        public CreateQueryApplicationContext(String pkg, String clsName) {
            super(pkg, clsName);
        }

        public void setViewTypeFull(String viewTypeFull){
            this.viewTypeFull = viewTypeFull;
            this.viewType = Utils.getType(viewTypeFull);
        }

        public void setQueryRepositoryTypeFull(String queryRepositoryTypeFull){
            this.queryRepositoryTypeFull = queryRepositoryTypeFull;
            this.queryRepositoryType = getType(queryRepositoryTypeFull);
        }

        public String getQueryRepositoryType() {
            return queryRepositoryType;
        }

        public String getQueryRepositoryTypeFull() {
            return queryRepositoryTypeFull;
        }

        public String getViewType() {
            return viewType;
        }

        public String getViewTypeFull() {
            return viewTypeFull;
        }
    }

}
