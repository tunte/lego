package com.geekhalo.lego.plugin.aggmethod;

import com.geekhalo.lego.plugin.support.action.BaseAnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.JavaPsiFacade;
import com.intellij.psi.PsiClass;
import com.intellij.psi.search.GlobalSearchScope;
import org.jetbrains.annotations.NotNull;

public class CreateAggregationMethodAction extends BaseAnAction {


    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        PsiClass psiClass = getJavaClass(e);

        if (psiClass == null) {
            String pkg = getPackage(e);
            VirtualFile packageAsFile = getPackageAsFile(e);
            Project project = e.getProject();
            String aggType = findAggFull(project, packageAsFile);
            if (aggType != null) {
                JavaPsiFacade javaPsiFacade = JavaPsiFacade.getInstance(project);
                GlobalSearchScope scope = GlobalSearchScope.allScope(project);
                psiClass = javaPsiFacade.findClass(aggType, scope);
            }

        }

        if (psiClass == null){
            Messages.showMessageDialog("请选择类", "Warn", null);
            return;
        }

        Module currentModule = findCurrentModule(e);
        Module[] modules = ModuleManager.getInstance(e.getProject()).getModules();
        Module appModule = findModule(modules, currentModule, APP_MODULE);
        Module domainModule = findModule(modules, currentModule, DOMAIN_MODULE);
        Module infraModule = findModule(modules, currentModule, INFRA_MODULE);


        CreateAggregationMethodDialog dialog = new CreateAggregationMethodDialog(e.getProject(),
                appModule, domainModule, infraModule,
                psiClass);
        dialog.show();
    }

}
