package com.geekhalo.lego.plugin.contextfactory;

import com.geekhalo.lego.plugin.support.action.BaseAnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.vfs.VirtualFile;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

public class CreateContextFactoryAction extends BaseAnAction {
    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        String pkg = getPackage(e);
        if (StringUtils.isEmpty(pkg)){
            Messages.showWarningDialog("选择正确的包名", "选择包");
            return;
        }

        VirtualFile[] filesInPackage = getFilesInPackage(e);
        String cmdType = findFullType(filesInPackage, name -> name.endsWith("Command"));
        String cxtType = findFullType(filesInPackage, name -> name.endsWith("Context"));
        Module currentModule = findCurrentModule(e);
        CreateContextFactoryDialog dialog = new CreateContextFactoryDialog(e.getProject(), currentModule, pkg, cmdType, cxtType);
        dialog.show();
    }


}
