package com.geekhalo.lego.plugin.contextfactory;

import com.geekhalo.lego.plugin.support.template.CreateClassContext;
import com.geekhalo.lego.plugin.support.util.Utils;

public class ContextFactoryTemplate {
    private static final String TEMPLATE =
            "package {pkg};\n" +
                    "\n" +
                    "import com.geekhalo.lego.core.command.support.handler.contextfactory.AbstractSmartContextFactory; \n" +
                    "import org.springframework.stereotype.Component;\n" +
                    "import lombok.extern.slf4j.Slf4j;\n" +
                    "import {commandTypeFull};\n" +
                    "import {contextTypeFull};\n" +

                    "\n" +
                    "@Component\n" +
                    "@Slf4j\n" +
                    "public class {className}\n" +
                    "        extends AbstractSmartContextFactory<{commandType}, {contextType}> {\n" +
                    "\n" +
                    "    public {className}(){\n" +
                    "        super({commandType}.class, {contextType}.class);\n" +
                    "    }\n" +
                    "\n" +
                    "    @Override\n" +
                    "    public {contextType} create({commandType} cmd) {\n" +
                    "        return {contextType}.apply(cmd);\n" +
                    "    }\n" +
                    "}";

    public static String create(CreateContextFactoryContext context){
        String content = TEMPLATE;
        content = content.replace("{pkg}", context.getPkg());
        content = content.replace("{className}", context.getClsName());
        content = content.replace("{commandType}", context.getCommandType());
        content = content.replace("{commandTypeFull}", context.getCommandTypeFull());

        content = content.replace("{contextType}", context.getContextType());
        content = content.replace("{contextTypeFull}", context.getContextTypeFull());

        return content;
    }
    public static class CreateContextFactoryContext extends CreateClassContext {
        private String commandType;
        private String commandTypeFull;
        private String contextType;
        private String contextTypeFull;

        public CreateContextFactoryContext(String pkg, String clsName) {
            super(pkg, clsName);
        }

        public void setCommandTypeFull(String commandTypeFull){
            this.commandTypeFull = commandTypeFull;
            this.commandType = Utils.getType(commandTypeFull);
        }

        public void setContextTypeFull(String contextTypeFull){
            this.contextTypeFull = contextTypeFull;
            this.contextType = Utils.getType(contextTypeFull);
        }

        public String getCommandType() {
            return commandType;
        }

        public String getCommandTypeFull() {
            return commandTypeFull;
        }

        public String getContextType() {
            return contextType;
        }

        public String getContextTypeFull() {
            return contextTypeFull;
        }
    }
}
