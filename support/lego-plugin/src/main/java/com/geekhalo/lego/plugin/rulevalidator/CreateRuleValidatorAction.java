package com.geekhalo.lego.plugin.rulevalidator;

import com.geekhalo.lego.plugin.support.action.BaseAnAction;
import com.geekhalo.lego.plugin.creator.JavaFileCreator;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.vfs.VirtualFile;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

public class CreateRuleValidatorAction extends BaseAnAction {
    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        String pkg = getPackage(e);
        if (StringUtils.isEmpty(pkg)){
            Messages.showWarningDialog("选择正确的包名", "选择包");
            return;
        }
        VirtualFile packageAsFile = getPackageAsFile(e);

        VirtualFile parent = packageAsFile.getParent();
        Project project = e.getProject();

        String appType = findAggFull(project, parent);
        if (StringUtils.isEmpty(appType)) {
            Messages.showWarningDialog("请选择具体的 聚合根", "Warning");
            return;
        }

        String clsName = Messages.showInputDialog(e.getProject(), "输入类名", "类名", null);
        if (StringUtils.isEmpty(clsName)) {
            Messages.showWarningDialog("请输入类名", "Warning");
            return;
        }

        Module currentModule = findCurrentModule(e);

        RuleValidatorTemplate.CreateRuleValidatorContext context = new RuleValidatorTemplate.CreateRuleValidatorContext(pkg,  clsName);
        context.setAggTypeFull(appType);

        String content = RuleValidatorTemplate.create(context);

        JavaFileCreator.createJavaFileInPackage(e.getProject(), currentModule, pkg, clsName, content);
    }
}
