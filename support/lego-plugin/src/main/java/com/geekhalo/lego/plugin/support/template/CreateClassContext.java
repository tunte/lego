package com.geekhalo.lego.plugin.support.template;


import static com.geekhalo.lego.plugin.support.util.Utils.getType;

public abstract class CreateClassContext {
    private final String pkg;
    private final String clsName;
    private String idType;
    private String idTypeFull;
    private String aggType;
    private String aggTypeFull;
    private String repositoryTypeFull;
    private String repositoryType;

    public CreateClassContext(String pkg, String clsName) {
        this.pkg = pkg;
        this.clsName = clsName;
    }

    public String getPkg() {
        return pkg;
    }

    public String getClsName() {
        return clsName;
    }

    public String getIdType() {
        return idType;
    }

    public String getIdTypeFull(){
        return idTypeFull;
    }

    public String getAggType() {
        return aggType;
    }

    public String getAggTypeFull() {
        return aggTypeFull;
    }

    public String getRepositoryTypeFull(){
        return repositoryTypeFull;
    }

    public String getRepositoryType(){
        return repositoryType;
    }

    public void setAggTypeFull(String aggTypeFull){
        this.aggTypeFull = aggTypeFull;
        this.aggType = getType(aggTypeFull);
    }

    public void setIdTypeFull(String idTypeFull) {
        this.idTypeFull = idTypeFull;
        this.idType = getType(idTypeFull);
    }

    public void setRepositoryTypeFull(String repositoryTypeFull) {
        this.repositoryTypeFull = repositoryTypeFull;
        this.repositoryType = getType(repositoryTypeFull);
    }

    public void setRepositoryType(String pkg, String type) {
        this.repositoryTypeFull = pkg + "." + type;
        this.repositoryType = type;
    }


}
