package com.geekhalo.lego.plugin.querymethod;

import com.geekhalo.lego.plugin.support.template.CreateClassContext;

public class QueryObjectTemplate {
    private static final String CONTEXT_TEMPLATE =
            "package {pkg};\n" +
            "\n" +
            "import lombok.Data;\n" +
            "import lombok.NoArgsConstructor;\n" +
            "\n" +
            "@NoArgsConstructor\n" +
            "@Data\n" +
            "public class {className}{\n" +
            "\n" +
            "\n" +
            "}\n";
    public static String create(CreateQueryObjectContext context){
        String content = CONTEXT_TEMPLATE;
        content = content.replace("{pkg}", context.getPkg());
        content = content.replace("{className}", context.getClsName());

        return content;
    }

    public static class CreateQueryObjectContext extends CreateClassContext {
        private String commandType;
        private String commandTypeFull;

        public CreateQueryObjectContext(String pkg, String clsName) {
            super(pkg, clsName);
        }
    }
}
