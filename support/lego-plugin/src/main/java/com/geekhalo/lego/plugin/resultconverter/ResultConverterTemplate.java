package com.geekhalo.lego.plugin.resultconverter;

import com.geekhalo.lego.plugin.support.template.CreateClassContext;
import com.geekhalo.lego.plugin.support.util.Utils;

public class ResultConverterTemplate {
    private static final String TEMPLATE =
            "package {pkg};\n" +
                    "\n" +
                    "import com.geekhalo.lego.core.command.support.handler.converter.AbstractSmartResultConverter;\n" +
                    "import org.springframework.stereotype.Component;\n" +
                    "import lombok.extern.slf4j.Slf4j;\n" +
                    "import {contextTypeFull};\n" +
                    "import {aggTypeFull};\n" +
                    "import {resultTypeFull};\n" +
                    "\n" +
                    "@Component\n" +
                    "@Slf4j\n" +
                    "public class {className}\n" +
                    "        extends AbstractSmartResultConverter<{aggType}, {contextType}, {resultType}> {\n" +
                    "\n" +
                    "    public {className}(){\n" +
                    "        super({aggType}.class, {contextType}.class, {resultType}.class);\n" +
                    "    }\n" +
                    "\n" +
                    "    @Override\n" +
                    "    public {resultType} convert({aggType} agg, {contextType} context) {\n" +
                    "        // 添加转换代码\n" +
                    "        return null;\n" +
                    "    }\n" +
                    "}";

    public static String create(CreateResultConverterContext context){
        String content = TEMPLATE;
        content = content.replace("{pkg}", context.getPkg());
        content = content.replace("{className}", context.getClsName());
        content = content.replace("{aggType}", context.getAggType());
        content = content.replace("{aggTypeFull}", context.getAggTypeFull());

        content = content.replace("{contextType}", context.getContextType());
        content = content.replace("{contextTypeFull}", context.getContextTypeFull());

        content = content.replace("{resultType}", context.getResultType());
        content = content.replace("{resultTypeFull}", context.getResultTypeFull());

        return content;
    }
    public static class CreateResultConverterContext extends CreateClassContext {
        private String aggType;
        private String aggTypeFull;
        private String contextType;
        private String contextTypeFull;
        private String resultType;
        private String resultTypeFull;

        public CreateResultConverterContext(String pkg, String clsName) {
            super(pkg, clsName);
        }

        public void setAggTypeFull(String aggTypeFull){
            this.aggTypeFull = aggTypeFull;
            this.aggType = Utils.getType(aggTypeFull);
        }

        public void setContextTypeFull(String contextTypeFull){
            this.contextTypeFull = contextTypeFull;
            this.contextType = Utils.getType(contextTypeFull);
        }

        public void setResultTypeFull(String resultTypeFull){
            this.resultTypeFull = resultTypeFull;
            this.resultType = Utils.getType(resultTypeFull);
        }

        @Override
        public String getAggType() {
            return aggType;
        }

        @Override
        public String getAggTypeFull() {
            return aggTypeFull;
        }

        public String getContextType() {
            return contextType;
        }

        public String getContextTypeFull() {
            return contextTypeFull;
        }

        public String getResultType() {
            return resultType;
        }

        public String getResultTypeFull() {
            return resultTypeFull;
        }
    }
}
