package com.geekhalo.lego.plugin.paramvalidator;

import com.geekhalo.lego.plugin.support.action.BaseAnAction;
import com.geekhalo.lego.plugin.creator.JavaFileCreator;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.vfs.VirtualFile;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

public class CreateParamValidatorAction extends BaseAnAction {
    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        String pkg = getPackage(e);
        if (StringUtils.isEmpty(pkg)){
            Messages.showWarningDialog("选择正确的包名", "选择包");
            return;
        }

        VirtualFile[] filesInPackage = getFilesInPackage(e);
        String cmdType = findFullType(filesInPackage, name -> name.endsWith("Command"));
        if (StringUtils.isEmpty(cmdType)) {
            Messages.showWarningDialog("请选择具体的 Command", "Warning");
            return;
        }

        String clsName = Messages.showInputDialog(e.getProject(), "输入类名", "类名", null);
        if (StringUtils.isEmpty(clsName)) {
            Messages.showWarningDialog("请输入类名", "Warning");
            return;
        }

        Module currentModule = findCurrentModule(e);

        ParamValidatorTemplate.CreateParamValidatorContext context = new ParamValidatorTemplate
                .CreateParamValidatorContext(pkg,  clsName);
        context.setCmdTypeFull(cmdType);

        String content = ParamValidatorTemplate.create(context);

        JavaFileCreator.createJavaFileInPackage(e.getProject(), currentModule, pkg, clsName, content);
    }
}
