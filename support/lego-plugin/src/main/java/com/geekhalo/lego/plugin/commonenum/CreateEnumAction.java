package com.geekhalo.lego.plugin.commonenum;

import com.geekhalo.lego.plugin.support.action.BaseAnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleManager;
import com.intellij.openapi.ui.Messages;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

public class CreateEnumAction extends BaseAnAction {
    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        String pkg = getPackage(e);
        if (StringUtils.isEmpty(pkg)){
            Messages.showWarningDialog("选择正确的包名", "选择包");
            return;
        }
        Module currentModule = findCurrentModule(e);
        Module[] modules = ModuleManager.getInstance(e.getProject()).getModules();
        Module domainModule = findModule(modules, currentModule, DOMAIN_MODULE);
        Module infraModule = findModule(modules, currentModule, INFRA_MODULE);

        CreateCommonEnumDialog dialog = new CreateCommonEnumDialog(e.getProject(), domainModule, infraModule, pkg);
        dialog.show();
    }
}
