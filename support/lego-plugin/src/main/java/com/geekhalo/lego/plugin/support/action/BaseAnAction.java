package com.geekhalo.lego.plugin.support.action;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleUtilCore;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.function.Function;

public abstract class BaseAnAction extends AnAction {
    public static final String APP_MODULE = "app";
    public static final String DOMAIN_MODULE = "domain";
    public static final String INFRA_MODULE = "infrastructure";

    protected Module findModule(Module[] modules, Module currentModule, String suffix) {
        String moduleName = currentModule.getName();
        int index = moduleName.lastIndexOf("-");
        String name = moduleName.substring(0, index);
        for (Module module : modules){
            if (module.getName().endsWith(name + "-" + suffix)){
                return module;
            }
        }
        return modules[0];
    }

    protected Module findCurrentModule(AnActionEvent e){
        VirtualFile currentFile = e.getData(CommonDataKeys.VIRTUAL_FILE);
        Module currentModule = ModuleUtilCore.findModuleForFile(currentFile, e.getProject());
        return currentModule;
    }

    protected String getPackage(AnActionEvent event){
        VirtualFile selectedFile = getPackageAsFile(event);
        return selectedFile == null ? null : getPackage(selectedFile);
    }

    protected VirtualFile getPackageAsFile(AnActionEvent event){
        // 获取当前选中的文件或目录
        VirtualFile[] selectedFiles =
                event.getData(PlatformDataKeys.VIRTUAL_FILE_ARRAY);
        // 如果没有文件或目录被选中，则返回
        if (selectedFiles == null || selectedFiles.length == 0) {
            return null;
        }
        return selectedFiles[0];
    }

    @NotNull
    private String getPackage(VirtualFile selectedFile) {
        // 获取选中文件或目录的路径
        String path = selectedFile.getPath();
        // 如果选中的是目录，则获取该目录的名称作为包名
        if (selectedFile.isDirectory()) {
            int index = path.indexOf("src/") + 4;
            path = path.substring(index);
        } else {
            // 如果选中的是文件，则获取该文件所在的目录的名称作为包名
            path = selectedFile.getParent().getPath();
            int index = path.indexOf("src/") + 4;
            path = path.substring(index);
        }
        // 替换路径中的斜杠为点号
        path = path.replace('/', '.');
        path = path.replace("main.java.","");

        // 输出选中的包名
        return path;
    }

    public String getFullType(VirtualFile file){
       return getPackage(file) + "." + file.getNameWithoutExtension();
    }

    protected VirtualFile[] getFilesInPackage(AnActionEvent event){
        // 获取当前选中的文件或目录
        VirtualFile[] selectedFiles =
                event.getData(PlatformDataKeys.VIRTUAL_FILE_ARRAY);
        // 如果没有文件或目录被选中，则返回
        if (selectedFiles == null || selectedFiles.length == 0) {
            return null;
        }
        // 获取选中文件或目录的路径
        VirtualFile pkg = selectedFiles[0];
        return pkg.getChildren();
    }


    protected PsiClass getJavaClass(AnActionEvent e){
        PsiFile psiFile = e.getData(CommonDataKeys.PSI_FILE);
        if (psiFile instanceof PsiJavaFile) {
            PsiJavaFile javaFile = (PsiJavaFile)psiFile;
            PsiClass[] classes = javaFile.getClasses();
            return classes[0];
        }
        return null;
    }

    protected String findFullType(VirtualFile[] files, Function<String, Boolean> function) {
        for (VirtualFile file: files) {
            if (function.apply(file.getNameWithoutExtension())) {
                return getFullType(file);
            }
        }
        return null;
    }

    @Nullable
    public String findAggFull(Project project, VirtualFile parent) {
        VirtualFile[] children = parent.getChildren();
        VirtualFile aggFile = findFile(children, file -> {
            if (file.isDirectory()){
                return false;
            }
            PsiFile psiFile = PsiManager.getInstance(project).findFile(file);
            if (psiFile instanceof PsiJavaFile) {
                PsiJavaFile javaFile = (PsiJavaFile) psiFile;
                for (PsiClass psiClass : javaFile.getClasses()) {
                    PsiReferenceList extendsList = psiClass.getExtendsList();
                    if (extendsList != null) {
                        for (PsiJavaCodeReferenceElement reference : extendsList.getReferenceElements()) {
                            PsiElement element = reference.resolve();
                            if (element instanceof PsiClass) {
                                PsiClass superClass = (PsiClass) element;
                                if ("com.geekhalo.lego.core.command.support.AbstractAggRoot".equals(superClass.getQualifiedName())) {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
            return false;
        });
        String appType = aggFile == null ? null : getFullType(aggFile);
        return appType;
    }

    protected VirtualFile findFile(VirtualFile[] files, Function<VirtualFile, Boolean> function){
        for(VirtualFile file : files){
            if (function.apply(file)){
                return file;
            }
        }
        return null;
    }
}
