package com.geekhalo.lego.plugin.sync;

import com.geekhalo.lego.plugin.support.action.BaseAnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.ui.Messages;
import com.intellij.psi.PsiClass;
import org.jetbrains.annotations.NotNull;

public class FieldSyncAction extends BaseAnAction {
    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        PsiClass targetPsiClass = getJavaClass(e);

        if (targetPsiClass == null){
            Messages.showMessageDialog("请选择查询服务", "Warn", null);
            return;
        }
        FieldSyncDialog dialog = new FieldSyncDialog(e.getProject(), targetPsiClass);
        dialog.show();
    }
}
