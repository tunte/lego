package com.geekhalo.lego.plugin.aggregation;

import com.geekhalo.lego.plugin.support.template.CreateClassContext;

import static com.geekhalo.lego.plugin.support.util.Utils.getType;

public class ViewTemplate {
    private static final String TEMPLATE =
            "package {pkg};\n" +
            "\n" +
            "import {parentClassFull};\n" +
            "import lombok.AccessLevel;\n" +
            "import lombok.Data;\n" +
            "import lombok.NoArgsConstructor;\n" +
            "import lombok.Setter;\n" +
            "import lombok.Getter;\n" +
            "\n" +
            "import javax.persistence.*;\n" +
            "\n" +
            "@Entity\n" +
            "@Table(name = \"{tableName}\")\n" +
            "@Data\n" +
            "@NoArgsConstructor(access = AccessLevel.PROTECTED)\n" +
            "@Setter(AccessLevel.PRIVATE)\n" +
            "public class {className} extends {parentClass} { \n" +
            "    @Id\n" +
            "    @GeneratedValue(strategy = GenerationType.IDENTITY)\n" +
            "    private Long id;" +
            "\n" +
            "\n" +
            "\n}";
    public static String create(CreateViewContext context) {
        String content = TEMPLATE;
        content = content.replace("{tableName}",context.getTableName());
        content = content.replace("{pkg}", context.getPkg());
        content = content.replace("{className}", context.getClsName());
        content = content.replace("{parentClassFull}", context.getParentClassFull());
        content = content.replace("{parentClass}", context.getParentClass());
        return content;
    }

    public static class CreateViewContext extends CreateClassContext {
        private String tableName;
        private String parentClassFull;
        private String parentClass;
        public CreateViewContext(String pkg, String clsName) {
            super(pkg, clsName);
        }

        public void setParentClassFull(String parentClassFull){
            this.parentClassFull = parentClassFull;
            this.parentClass = getType(parentClassFull);
        }

        public String getParentClassFull() {
            return parentClassFull;
        }

        public String getParentClass() {
            return parentClass;
        }

        public String getTableName() {
            return tableName;
        }

        public void setTableName(String tableName) {
            this.tableName = tableName;
        }
    }
}
