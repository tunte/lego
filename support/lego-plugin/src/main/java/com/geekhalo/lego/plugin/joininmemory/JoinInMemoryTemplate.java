package com.geekhalo.lego.plugin.joininmemory;

import com.geekhalo.lego.plugin.support.template.CreateClassContext;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class JoinInMemoryTemplate {
    private static final String TEMPLATE_1 =
            "package {pkg};\n" +
            "\n" +
            "import com.geekhalo.lego.annotation.joininmemory.JoinInMemory;\n" +
            "import org.springframework.core.annotation.AliasFor;\n" +
            "\n" +
            "import java.lang.annotation.ElementType;\n" +
            "import java.lang.annotation.Retention;\n" +
            "import java.lang.annotation.RetentionPolicy;\n" +
            "import java.lang.annotation.Target;\n" +
            "\n" +
            "@Target(ElementType.FIELD)\n" +
            "@Retention(RetentionPolicy.RUNTIME)\n";
    private static final String TEMPLATE_2 =
            "@JoinInMemory(keyFromSourceData = \"\",\n" +
            "        keyFromJoinData = \"#{id}\",\n" +
            "        loader = \"#{@{beanName}.{loadMethodName}(#root)}\"\n" +
            ")\n" +
            "public @interface {className} {\n" +
            "    @AliasFor(\n" +
            "            annotation = JoinInMemory.class\n" +
            "    )\n" +
            "    String keyFromSourceData();\n" +
            "}\n";

    public static String createShowView(CreateJoinInMemoryContext context){
        return replace(TEMPLATE_2, context);
    }

    public static String create(CreateJoinInMemoryContext context){
        return replace(TEMPLATE_1 + TEMPLATE_2, context);
    }

    @NotNull
    private static String replace(String content, CreateJoinInMemoryContext context) {
        content = content.replace("{pkg}", context.getPkg());
        content = content.replace("{className}", context.getClsName());
        content = content.replace("{beanName}", context.getBeanName());
        content = content.replace("{loadMethodName}", context.getLoadMethodName());
        return content;
    }


    public static class CreateJoinInMemoryContext extends CreateClassContext {
        private String beanName;
        private List<String> parameters;
        private String loadMethodName;
        public CreateJoinInMemoryContext(String pkg, String clsName) {
            super(pkg, clsName);
        }

        public String getBeanName() {
            return beanName;
        }

        public void setBeanName(String beanName) {
            this.beanName = beanName;
        }

        public List<String> getParameters() {
            return parameters;
        }

        public void setParameters(List<String> parameters) {
            this.parameters = parameters;
        }

        public String getLoadMethodName() {
            return loadMethodName;
        }

        public void setLoadMethodName(String loadMethodName) {
            this.loadMethodName = loadMethodName;
        }

    }
}
