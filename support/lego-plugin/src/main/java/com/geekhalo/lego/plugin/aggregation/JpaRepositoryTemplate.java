package com.geekhalo.lego.plugin.aggregation;

import com.geekhalo.lego.plugin.support.template.CreateClassContext;
import com.geekhalo.lego.plugin.support.util.Utils;

public class JpaRepositoryTemplate {
    private static final String COMMAND_TEMPLATE =
            "package {pkg};\n" +
            "\n" +
            "\n" +
            "import org.springframework.data.jpa.repository.JpaRepository;\n" +
            "import org.springframework.stereotype.Repository;\n" +
            "import {repositoryTypeFull};\n" +
            "import {aggTypeFull};\n" +
            "import {idTypeFull};\n" +
            "\n" +
            "@Repository(\"{beanName}\")\n" +
            "public interface {className} " +
                    "extends {repositoryType},JpaRepository<{aggType}, {idType}> {\n" +
            "    @Override\n" +
            "    default {aggType} sync({aggType} agg){\n" +
            "        return save(agg);\n" +
            "    }"+
            "\n" +
            "\n" +
            "\n" +
            "}\n";
    private static final String QUERY_TEMPLATE =
            "package {pkg};\n" +
            "\n" +
            "\n" +
            "import org.springframework.data.jpa.repository.JpaRepository;\n" +
            "import org.springframework.stereotype.Repository;\n" +
            "import {repositoryTypeFull};\n" +
            "import {viewTypeFull};\n" +
            "import {viewIdTypeFull};\n" +
            "\n" +
            "@Repository(\"{beanName}\")\n" +
            "public interface {className} " +
                    "extends {repositoryType}, JpaRepository<{viewType}, {viewIdType}> {\n" +
            "\n" +
            "\n" +
            "\n" +
            "}\n";

    public static String createCommand(CreateJpaCommandRepositoryContext context) {
        String content = COMMAND_TEMPLATE;
        content = content.replace("{repositoryTypeFull}", context.getRepositoryTypeFull());
        content = content.replace("{repositoryType}", context.getRepositoryType());
        content = content.replace("{beanName}", Utils.getBeanName(context.getRepositoryType()));
        content = content.replace("{pkg}", context.getPkg());
        content = content.replace("{className}", context.getClsName());
        content = content.replace("{aggType}", context.getAggType());
        content = content.replace("{aggTypeFull}", context.getAggTypeFull());
        content = content.replace("{idType}", context.getIdType());
        content = content.replace("{idTypeFull}", context.getIdTypeFull());
        return content;
    }

    public static String createQuery(CreateJpaQueryRepositoryContext context) {
        String content = QUERY_TEMPLATE;
        content = content.replace("{repositoryTypeFull}", context.getRepositoryTypeFull());
        content = content.replace("{repositoryType}", context.getRepositoryType());
        content = content.replace("{beanName}", Utils.getBeanName(context.getRepositoryType()));
        content = content.replace("{pkg}", context.getPkg());
        content = content.replace("{className}", context.getClsName());
//        content = content.replace("{aggType}", context.getAggType());
//        content = content.replace("{aggTypeFull}", context.getAggTypeFull());
//        content = content.replace("{idType}", context.getIdType());
//        content = content.replace("{idTypeFull}", context.getIdTypeFull());

        content = content.replace("{viewType}", context.getViewType());
        content = content.replace("{viewTypeFull}", context.getViewTypeFull());
        content = content.replace("{viewIdType}", context.getViewIdType());
        content = content.replace("{viewIdTypeFull}", context.getViewIdTypeFull());
        return content;
    }

    public static class CreateJpaCommandRepositoryContext extends CreateClassContext {

        public CreateJpaCommandRepositoryContext(String pkg, String clsName) {
            super(pkg, clsName);
        }
    }

    public static class CreateJpaQueryRepositoryContext extends CreateClassContext{
        private String repositoryTypeFull;
        private String repositoryType;
        private String viewType;
        private String viewTypeFull;
        private String viewIdType;
        private String viewIdTypeFull;

        public CreateJpaQueryRepositoryContext(String pkg, String clsName) {
            super(pkg, clsName);
        }

        public void setViewTypeFull(String viewTypeFull){
            this.viewTypeFull = viewTypeFull;
            this.viewType = Utils.getType(viewTypeFull);
        }

        public void setViewIdTypeFull(String viewIdTypeFull){
            this.viewIdTypeFull = viewIdTypeFull;
            this.viewIdType = Utils.getType(viewIdTypeFull);
        }

        public String getViewType() {
            return viewType;
        }

        public String getViewTypeFull() {
            return viewTypeFull;
        }

        public String getViewIdType() {
            return viewIdType;
        }

        public String getViewIdTypeFull() {
            return viewIdTypeFull;
        }

    }
}
