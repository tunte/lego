package com.geekhalo.lego.plugin.entity;

import com.geekhalo.lego.plugin.support.action.BaseAnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import org.jetbrains.annotations.NotNull;

public class CreateEntityAction extends BaseAnAction {
    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        String pkg = getPackage(e);
        if (pkg == null){
            Messages.showMessageDialog("请选择包", "Warn", null);
            return;
        }
        Project project = e.getData(PlatformDataKeys.PROJECT);
        Module currentModule = findCurrentModule(e);

        Module[] modules = ModuleManager.getInstance(project).getModules();

        Module appModule = findModule(modules, currentModule, APP_MODULE);
        Module domainModule = findModule(modules, currentModule, DOMAIN_MODULE);
        Module infraModule = findModule(modules, currentModule, INFRA_MODULE);

        CreateEntityDialog dialog = new CreateEntityDialog(project, pkg, appModule, domainModule, infraModule);
        dialog.show();
    }



}
