package com.geekhalo.lego.plugin.sync;

import com.intellij.codeInsight.daemon.GutterIconNavigationHandler;
import com.intellij.codeInsight.daemon.LineMarkerInfo;
import com.intellij.codeInsight.daemon.LineMarkerProvider;
import com.intellij.openapi.editor.markup.GutterIconRenderer;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.IconLoader;
import com.intellij.psi.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.awt.event.MouseEvent;
import java.util.Collection;
import java.util.List;

public class FieldSyncLineMarkerProvider implements LineMarkerProvider {
    @Nullable
    @Override
    public LineMarkerInfo getLineMarkerInfo(@NotNull PsiElement psiElement) {
        if (psiElement instanceof PsiClass){
            return new LineMarkerInfo(psiElement, psiElement.getTextRange(),
                    IconLoader.findIcon("/images/sync_3.png"),
                    e -> "将字段同步到其他类",
                    new ClassSyncNavigationHandler(),
                    GutterIconRenderer.Alignment.LEFT,
                    ()-> "将字段同步到其他类"
            );
        }
        if (psiElement instanceof PsiField){
            return new LineMarkerInfo(psiElement, psiElement.getTextRange(),
                    IconLoader.findIcon("/images/sync_3.png"),
                    e -> "将字段同步到其他类",
                    new FieldSyncNavigationHandler(),
                    GutterIconRenderer.Alignment.LEFT,
                    ()-> "将字段同步到其他类"
                    );
        }
        return null;
    }

    class FieldSyncNavigationHandler implements GutterIconNavigationHandler {

        @Override
        public void navigate(MouseEvent e, PsiElement elt) {
            FieldSyncDialog dialog = new FieldSyncDialog(elt.getProject(), (PsiField) elt);
            dialog.show();
        }
    }

    class ClassSyncNavigationHandler implements GutterIconNavigationHandler {

        @Override
        public void navigate(MouseEvent e, PsiElement elt) {
            FieldSyncDialog dialog = new FieldSyncDialog(elt.getProject(), (PsiClass) elt);
            dialog.show();
        }
    }

    @Override
    public void collectSlowLineMarkers(@NotNull List element, @NotNull Collection collection) {

    }

}