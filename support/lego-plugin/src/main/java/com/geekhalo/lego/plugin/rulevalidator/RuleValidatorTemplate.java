package com.geekhalo.lego.plugin.rulevalidator;

import com.geekhalo.lego.plugin.support.template.CreateClassContext;
import com.geekhalo.lego.plugin.support.util.Utils;

public class RuleValidatorTemplate {
    private static final String TEMPLATE =
            "package {pkg};\n" +
            "\n" +
            "import com.geekhalo.lego.common.validator.ValidateErrorHandler;\n" +
            "import com.geekhalo.lego.core.validator.AbstractRuleValidator;\n" +
            "import {aggTypeFull};\n" +
            "import lombok.extern.slf4j.Slf4j;\n" +
            "import org.springframework.stereotype.Component;\n" +
            "\n" +
            "@Component\n" +
            "@Slf4j\n" +
            "public class {className}\n" +
            "    extends AbstractRuleValidator<{aggType}> {\n" +
            "\n" +
            "    public {className}(){\n" +
            "        super({aggType}.class);\n" +
            "    }\n" +
            "\n" +
            "    @Override\n" +
            "    public void validate({aggType} agg, ValidateErrorHandler validateErrorHandler) {\n" +
            "        log.info(\"validate(agg) {}\", agg);\n" +
            "    }\n" +
            "}\n";
    public static String create(CreateRuleValidatorContext context){
        String content = TEMPLATE;
        content = content.replace("{pkg}", context.getPkg());
        content = content.replace("{className}", context.getClsName());
        content = content.replace("{aggTypeFull}", context.getAggTypeFull());
        content = content.replace("{aggType}", context.getAggType());


        return content;
    }

    public static class CreateRuleValidatorContext extends CreateClassContext {
        private String aggTypeFull;
        private String aggType;
        public CreateRuleValidatorContext(String pkg, String clsName) {
            super(pkg, clsName);
        }

        public void setAggTypeFull(String aggTypeFull) {
            this.aggTypeFull = aggTypeFull;
            this.aggType = Utils.getType(aggTypeFull);
        }
        public String getAggTypeFull() {
            return aggTypeFull;
        }

        public String getAggType() {
            return aggType;
        }
    }
}
