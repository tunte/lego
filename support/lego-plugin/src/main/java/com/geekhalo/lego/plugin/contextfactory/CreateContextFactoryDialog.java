package com.geekhalo.lego.plugin.contextfactory;

import com.geekhalo.lego.plugin.creator.JavaFileCreator;
import com.geekhalo.lego.plugin.support.util.Utils;
import com.intellij.ide.util.ClassFilter;
import com.intellij.ide.util.TreeClassChooser;
import com.intellij.ide.util.TreeClassChooserFactory;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.Messages;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiModifier;
import com.intellij.psi.search.GlobalSearchScope;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class CreateContextFactoryDialog extends DialogWrapper {
    private JPanel contentPane;
    private JTextField cmdClassName;
    private JButton selectCmdTypeBtn;
    private JTextField cxtClassName;
    private JButton selectContextTypeBtn;
    private JTextField factoryName;

    private final Project project;
    private final Module module;
    private final String pkg;
    private String cmdTypeFull;
    private String cxtTypeFull;

    public CreateContextFactoryDialog(Project project, Module module, String pkg,
                                      String cmdTypeFull, String cxtTypeFull) {
        super(project);
        this.project = project;
        this.module = module;
        this.pkg = pkg;
        this.cmdTypeFull = cmdTypeFull;
        this.cxtTypeFull = cxtTypeFull;
        setModal(true);

        initComponent();
        super.init();
    }

    private void initComponent() {
        if (StringUtils.isNotEmpty(this.cmdTypeFull)) {
            this.cmdClassName.setText(this.cmdTypeFull);
        }
        if (StringUtils.isNotEmpty(this.cxtTypeFull)) {
            this.cxtClassName.setText(this.cxtTypeFull);
            this.factoryName.setText(Utils.createFactoryName(this.cxtTypeFull));
        }

        // 选择Command类
        this.selectCmdTypeBtn.addActionListener(e->{
            TreeClassChooserFactory factory = TreeClassChooserFactory.getInstance(project);
            // 设置过滤条件，只选择 public 类型的类
            ClassFilter classFilter = psiClass -> psiClass.getModifierList().hasExplicitModifier(PsiModifier.PUBLIC);
            // 弹出选择类的窗口
            TreeClassChooser chooser = factory.createWithInnerClassesScopeChooser(
                    "选择Command类",
                    GlobalSearchScope.allScope(project),
                    classFilter,
                    null);
            chooser.showDialog();
            // 获取用户选择的类
            PsiClass selectedClass = chooser.getSelected();
            // 如果用户选择了类，则在控制台输出类名
            if (selectedClass != null) {
                this.cmdClassName.setText(selectedClass.getQualifiedName());
            }
        });

        // 选择 Context 类
        this.selectContextTypeBtn.addActionListener(e->{
            TreeClassChooserFactory factory = TreeClassChooserFactory.getInstance(project);
            // 设置过滤条件，只选择 public 类型的类
            ClassFilter classFilter = psiClass -> psiClass.getModifierList().hasExplicitModifier(PsiModifier.PUBLIC);
            // 弹出选择类的窗口
            TreeClassChooser chooser = factory.createWithInnerClassesScopeChooser(
                    "选择Context类",
                    GlobalSearchScope.allScope(project),
                    classFilter,
                    null);
            chooser.showDialog();
            // 获取用户选择的类
            PsiClass selectedClass = chooser.getSelected();
            // 如果用户选择了类，则在控制台输出类名
            if (selectedClass != null) {
                this.cxtClassName.setText(selectedClass.getQualifiedName());
            }
        });

    }

    @Override
    protected @Nullable JComponent createCenterPanel() {
        return contentPane;
    }
    @Override
    protected void doOKAction(){
        createFiles();
        super.doOKAction();
    }

    @Override
    public void doCancelAction(){
        super.doCancelAction();
    }

    private void createFiles() {
        if (StringUtils.isEmpty(this.factoryName.getText())){
            Messages.showMessageDialog("类名不能为null", "ERROR", null);
            return;
        }
        if (StringUtils.isEmpty(this.cmdClassName.getText())){
            Messages.showMessageDialog("Command不能为null", "ERROR", null);
            return;
        }
        if (StringUtils.isEmpty(this.cxtClassName.getText())){
            Messages.showMessageDialog("Context不能为null", "ERROR", null);
            return;
        }

        ContextFactoryTemplate.CreateContextFactoryContext context =
                new ContextFactoryTemplate.CreateContextFactoryContext(this.pkg, this.factoryName.getText());
        context.setCommandTypeFull(this.cmdClassName.getText());
        context.setContextTypeFull(this.cxtClassName.getText());

        String content = ContextFactoryTemplate.create(context);
        JavaFileCreator.createJavaFileInPackage(this.project,
                this.module, this.pkg, this.factoryName.getText(), content);

    }
}
