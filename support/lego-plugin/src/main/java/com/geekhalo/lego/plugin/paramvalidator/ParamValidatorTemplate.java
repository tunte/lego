package com.geekhalo.lego.plugin.paramvalidator;

import com.geekhalo.lego.plugin.support.template.CreateClassContext;
import com.geekhalo.lego.plugin.support.util.Utils;

public class ParamValidatorTemplate {
    private static final String TEMPLATE =
            "package {pkg};\n" +
            "\n" +
            "import com.geekhalo.lego.common.validator.ValidateErrorHandler;\n" +
            "import com.geekhalo.lego.core.validator.AbstractParamValidator;\n" +
            "import {cmdTypeFull};\n" +
            "import lombok.extern.slf4j.Slf4j;\n" +
            "import org.springframework.stereotype.Component;\n" +
            "\n" +
            "@Component\n" +
            "@Slf4j\n" +
            "public class {className}\n" +
            "    extends AbstractParamValidator<{cmdType}> {\n" +
            "\n" +
            "    public {className}(){\n" +
            "        super({cmdType}.class);\n" +
            "    }\n" +
            "\n" +
            "    @Override\n" +
            "    public void validate({cmdType} cmd, ValidateErrorHandler validateErrorHandler) {\n" +
            "        log.info(\"validate(cmd) {}\", cmd);\n" +
            "    }\n" +
            "}\n";
    public static String create(CreateParamValidatorContext context){
        String content = TEMPLATE;
        content = content.replace("{pkg}", context.getPkg());
        content = content.replace("{className}", context.getClsName());
        content = content.replace("{cmdTypeFull}", context.getCmdTypeFull());
        content = content.replace("{cmdType}", context.getCmdType());


        return content;
    }

    public static class CreateParamValidatorContext extends CreateClassContext {
        private String cmdTypeFull;
        private String cmdType;
        public CreateParamValidatorContext(String pkg, String clsName) {
            super(pkg, clsName);
        }

        public void setCmdTypeFull(String cmdTypeFull) {
            this.cmdTypeFull = cmdTypeFull;
            this.cmdType = Utils.getType(cmdTypeFull);
        }
        public String getCmdTypeFull() {
            return cmdTypeFull;
        }

        public String getCmdType() {
            return cmdType;
        }
    }
}
