package com.geekhalo.lego.plugin.aggregation;


import com.geekhalo.lego.plugin.aggmethod.DomainEventTemplate;
import com.geekhalo.lego.plugin.creator.JavaFileCreator;
import com.geekhalo.lego.plugin.support.template.CreateClassContext;
import com.geekhalo.lego.plugin.support.ui.DocumentUpdateListener;
import com.geekhalo.lego.plugin.support.util.Utils;
import com.intellij.ide.util.ClassFilter;
import com.intellij.ide.util.PackageChooserDialog;
import com.intellij.ide.util.TreeClassChooser;
import com.intellij.ide.util.TreeClassChooserFactory;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiModifier;
import com.intellij.psi.PsiPackage;
import com.intellij.psi.search.GlobalSearchScope;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.awt.event.*;

import static com.geekhalo.lego.plugin.support.util.Utils.*;

public class CreateAggregationDialog extends DialogWrapper {
    private Project project;
    private Module appModule;
    private Module domainModule;
    private Module infraModule;

    private JPanel contentPane;
    private JButton buttonOK;
    private JPanel aggPanel;
    private JPanel repositoryPanel;
    private JTextField aggPackage;
    private JButton selectPkgForAgg;
    private JTextField aggClassName;
    private JTextField aggParentClass;
    private JButton aggParentSelectButton;
    private JTextField repositoryPkg;
    private JButton repositoryPackageSelectButton;
    private JTextField commandRepository;
    private JTextField queryRepository;
    private JPanel applicationService;
    private JTextField applicationPkg;
    private JButton applicationPackageSelectButton;
    private JTextField commandApplication;
    private JTextField queryApplication;
    private JPanel domainEvent;
    private JTextField domainEventPkg;
    private JButton domainEventPackageSelectButton;
    private JTextField domainEventClass;
    private JTextField aggIdTextField;
    private JButton aggIdTypeSelectButton;
    private JTextField aggModuleName;
    private JTextField repositoryModule;
    private JCheckBox createQueryRepository;
    private JCheckBox createCommandRepository;
    private JTextField applicationModule;
    private JCheckBox createCommandApplication;
    private JCheckBox createQueryApplication;
    private JTextField abstractDomainEventModule;
    private JCheckBox createAbstractDomainEvent;
    private JPanel jpaPanel;
    private JTextField jpaModule;
    private JTextField jpaRepositoryPkg;
    private JButton selectJpaRepositoryPkgBtn;
    private JCheckBox jpaCmdRepositoryEnable;
    private JTextField jpaCmdRepositoryImplClassName;
    private JCheckBox jpaQueryRepositoryEnable;
    private JTextField jpaQueryRepositoryImplClassName;
    private JTabbedPane tabbedPane1;
    private JTextField viewModule;
    private JTextField viewPkg;
    private JButton selectViewPkgBtn;
    private JTextField viewClassName;
    private JTextField queyAppModule;
    private JTextField queryApplicationPkg;
    private JButton selectQueryAppPkg;
    private JTextField queryRepositoryModule;
    private JTextField queryRepositoryPkg;
    private JButton selectQueryRepositoryPkgBtn;
    private JTextField jpaQueryRepositoryModule;
    private JTextField jpaQueryRepositoryPkg;
    private JButton selectJpaQueryRepositoryPkgBtn;
    private JTextField viewParentClass;
    private JButton selectViewParentClassBtn;

    private void init(Project project, String pkg){
        this.project = project;
        // 绑定模块信息
        bindModules();
        // 更新 包信息
        updateByPackage(pkg);

        // 聚合根包改变，更新其他包
        this.aggPackage.getDocument()
                .addDocumentListener(new DocumentUpdateListener(this::updateByPackage));

        registerPkgSelect(project);

        this.aggClassName.getDocument()
                .addDocumentListener(new DocumentUpdateListener(this::onAggNameChanged));
        this.viewClassName.getDocument()
                .addDocumentListener(new DocumentUpdateListener(this::refreshQueryByViewNameChanged));

        this.queryRepository.getDocument()
                .addDocumentListener(new DocumentUpdateListener(
                        r -> this.jpaQueryRepositoryImplClassName.setText(Utils.createJpaQueryRepositoryImplByRepository(r))));
        this.commandRepository.getDocument()
                .addDocumentListener(new DocumentUpdateListener(
                        r -> this.jpaCmdRepositoryImplClassName.setText(Utils.createJpaCmdRepositoryImplByRepository(r))));
    }

    private void registerPkgSelect(Project project) {
        // 选择 Agg 所在包
        selectPkgForAgg.addActionListener(e -> {
            PackageChooserDialog chooserDialog = new PackageChooserDialog("请选择包", project);
            if(chooserDialog.showAndGet()) {
                PsiPackage selectedPackage = chooserDialog.getSelectedPackage();
                if (selectedPackage != null) {
                    aggPackage.setText(selectedPackage.getQualifiedName());
                }
            }
        });

        // 选择 Infra 所在包
        selectJpaRepositoryPkgBtn.addActionListener(e -> {
            PackageChooserDialog chooserDialog = new PackageChooserDialog("请选择包", project);
            if(chooserDialog.showAndGet()) {
                PsiPackage selectedPackage = chooserDialog.getSelectedPackage();
                if (selectedPackage != null) {
                    jpaRepositoryPkg.setText(selectedPackage.getQualifiedName());
                }
            }
        });

        // 选择 Agg 父类
        this.aggParentSelectButton.addActionListener(e -> {
            TreeClassChooserFactory factory = TreeClassChooserFactory.getInstance(project);
            // 设置过滤条件，只选择 public 类型的类
            ClassFilter classFilter = psiClass -> psiClass.getModifierList().hasExplicitModifier(PsiModifier.PUBLIC);
            // 弹出选择类的窗口
            TreeClassChooser chooser = factory.createWithInnerClassesScopeChooser("选择聚合根父类", GlobalSearchScope.allScope(project), classFilter, null);
            chooser.showDialog();
            // 获取用户选择的类
            PsiClass selectedClass = chooser.getSelected();
            // 如果用户选择了类，则在控制台输出类名
            if (selectedClass != null) {
                aggParentClass.setText(selectedClass.getQualifiedName());
            }
        });

        // 选择 Agg Id 类型
        this.aggIdTypeSelectButton.addActionListener(e->{
            TreeClassChooserFactory factory = TreeClassChooserFactory.getInstance(project);
            // 设置过滤条件，只选择 public 类型的类
            ClassFilter classFilter = psiClass -> psiClass.getModifierList().hasExplicitModifier(PsiModifier.PUBLIC);
            // 弹出选择类的窗口
            TreeClassChooser chooser = factory.createWithInnerClassesScopeChooser("选择聚合根主键", GlobalSearchScope.allScope(project), classFilter, null);
            chooser.showDialog();
            // 获取用户选择的类
            PsiClass selectedClass = chooser.getSelected();
            // 如果用户选择了类，则在控制台输出类名
            if (selectedClass != null) {
                aggIdTextField.setText(selectedClass.getQualifiedName());
            }
        });

        // 选择 repository 所在的包
        this.repositoryPackageSelectButton.addActionListener(e->{
            PackageChooserDialog chooserDialog = new PackageChooserDialog("请选择包", project);
            if(chooserDialog.showAndGet()) {
                PsiPackage selectedPackage = chooserDialog.getSelectedPackage();
                if (selectedPackage != null) {
                    repositoryPkg.setText(selectedPackage.getQualifiedName());
                }
            }
        });

        // 选择 application 所在包
        this.applicationPackageSelectButton.addActionListener(e->{
            PackageChooserDialog chooserDialog = new PackageChooserDialog("请选择包", project);
            if(chooserDialog.showAndGet()) {
                PsiPackage selectedPackage = chooserDialog.getSelectedPackage();
                if (selectedPackage != null) {
                    applicationPkg.setText(selectedPackage.getQualifiedName());
                }
            }
        });

        // 选择 领域事件所在包
        this.domainEventPackageSelectButton.addActionListener(e->{
            PackageChooserDialog chooserDialog = new PackageChooserDialog("请选择包", project);
            if(chooserDialog.showAndGet()) {
                PsiPackage selectedPackage = chooserDialog.getSelectedPackage();
                if (selectedPackage != null) {
                    domainEventPkg.setText(selectedPackage.getQualifiedName());
                }
            }
        });

        // 选择 视图包
        this.selectViewPkgBtn.addActionListener(e->{
            PackageChooserDialog chooserDialog = new PackageChooserDialog("请选择包", project);
            if(chooserDialog.showAndGet()) {
                PsiPackage selectedPackage = chooserDialog.getSelectedPackage();
                if (selectedPackage != null) {
                    viewPkg.setText(selectedPackage.getQualifiedName());
                }
            }
        });

        // 选择视图模型 父类
        this.selectViewParentClassBtn.addActionListener(e->{
            PackageChooserDialog chooserDialog = new PackageChooserDialog("请选择包", project);
            if(chooserDialog.showAndGet()) {
                PsiPackage selectedPackage = chooserDialog.getSelectedPackage();
                if (selectedPackage != null) {
                    viewParentClass.setText(selectedPackage.getQualifiedName());
                }
            }
        });

        // 选择 Query Application 包
        this.selectQueryAppPkg.addActionListener(e->{
            PackageChooserDialog chooserDialog = new PackageChooserDialog("请选择包", project);
            if(chooserDialog.showAndGet()) {
                PsiPackage selectedPackage = chooserDialog.getSelectedPackage();
                if (selectedPackage != null) {
                    queryApplicationPkg.setText(selectedPackage.getQualifiedName());
                }
            }
        });

        // 选择 Query Repository 包
        this.selectQueryRepositoryPkgBtn.addActionListener(e->{
            PackageChooserDialog chooserDialog = new PackageChooserDialog("请选择包", project);
            if(chooserDialog.showAndGet()) {
                PsiPackage selectedPackage = chooserDialog.getSelectedPackage();
                if (selectedPackage != null) {
                    queryRepositoryPkg.setText(selectedPackage.getQualifiedName());
                }
            }
        });

        // 选择 Jpa Query 实现 包
        this.selectJpaQueryRepositoryPkgBtn.addActionListener(e->{
            PackageChooserDialog chooserDialog = new PackageChooserDialog("请选择包", project);
            if(chooserDialog.showAndGet()) {
                PsiPackage selectedPackage = chooserDialog.getSelectedPackage();
                if (selectedPackage != null) {
                    jpaQueryRepositoryPkg.setText(selectedPackage.getQualifiedName());
                }
            }
        });
    }

    @Override
    protected void doOKAction() {
        // 在这里添加确认按钮的自定义行为
        super.doOKAction();
        createFiles();
        dispose();
    }

    @Override
    public void doCancelAction() {
        super.doCancelAction();
        dispose();
    }
    /**
     * 绑定 Module <br />
     */
    private void bindModules() {
        this.aggModuleName.setText(this.domainModule.getName());
        this.repositoryModule.setText(this.domainModule.getName());
        this.applicationModule.setText(this.appModule.getName());
        this.abstractDomainEventModule.setText(this.domainModule.getName());
        this.jpaModule.setText(this.infraModule.getName());

        this.viewModule.setText(this.domainModule.getName());
        this.queryRepositoryModule.setText(this.domainModule.getName());
        this.queyAppModule.setText(this.appModule.getName());
        this.jpaQueryRepositoryModule.setText(this.infraModule.getName());
    }

    /**
     * 更新 pkg
     * @param pkg
     */
    private void updateByPackage(String pkg) {
        if (StringUtils.isNotEmpty(pkg)) {
            aggPackage.setText(pkg);
            repositoryPkg.setText(pkg);
            domainEventPkg.setText(pkg);

            String appPkg = Utils.domainPkgToApp(pkg);
            applicationPkg.setText(appPkg);

            String infraPkg = Utils.domainPkgToInfra(pkg);
            this.jpaRepositoryPkg.setText(infraPkg);

            this.viewPkg.setText(pkg);
            this.queryRepositoryPkg.setText(pkg);
            this.queryApplicationPkg.setText(Utils.domainPkgToApp(pkg));
            this.jpaQueryRepositoryPkg.setText(Utils.domainPkgToInfra(pkg));
        }

    }

    private void onAggNameChanged(String agg){
        this.refreshCommandByAggNameChanged(agg);
        this.viewClassName.setText(Utils.createViewByAgg(agg));
        this.refreshQueryByViewNameChanged(this.viewClassName.getText());
    }

    private void refreshCommandByAggNameChanged(String aggClass){
        this.domainEventClass.setText(createAbstractDomainEventByAgg(aggClass));

        this.commandRepository.setText(createCommandRepositoryByAgg(aggClass));

        this.commandApplication.setText(createCommandApplicationByAgg(aggClass));

        this.jpaCmdRepositoryImplClassName
                .setText(Utils.createJpaCmdRepositoryImplByRepository(this.commandRepository.getText()));

    }

    private void refreshQueryByViewNameChanged(String viewName) {
        this.queryRepository.setText(createQueryRepositoryByView(viewName));

        this.queryApplication.setText(createQueryApplicationByView(viewName));

        this.jpaQueryRepositoryImplClassName
                .setText(Utils.createJpaQueryRepositoryImplByRepository(this.queryRepository.getText()));
    }


    public CreateAggregationDialog(Project project, String pkg, Module appModule, Module domainModule, Module infraModule) {
        super(project);
        this.appModule = appModule;
        this.domainModule = domainModule;
        this.infraModule = infraModule;

        setModal(true);
        getRootPane().setDefaultButton(buttonOK);


        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                doCancelAction();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        init(project, pkg);
        init();
    }

    private void createFiles(){
        // 创建 聚合根
        {
            AggregationTemplate.CreateAggregationContext context = new AggregationTemplate.CreateAggregationContext(this.aggPackage.getText(), this.aggClassName.getText());
            bindCommon(context);
            context.setParentClassFull(aggParentClass.getText());
            context.setTableName(Utils.createTableByEntity(this.aggClassName.getText()));
            String content = AggregationTemplate.create(context);
            JavaFileCreator.createJavaFileInPackage(this.project, this.domainModule,
                    this.aggPackage.getText(), this.aggClassName.getText(), content);
        }

        // 创建 AbstractDomainEvent
        if (createAbstractDomainEvent.isSelected()){
            DomainEventTemplate.CreateAbstractDomainEventContext context = new DomainEventTemplate.CreateAbstractDomainEventContext(this.domainEventPkg.getText(), this.domainEventClass.getText());
            bindCommon(context);
            String domainContent = DomainEventTemplate.createAbstractEvent(context);
            JavaFileCreator.createJavaFileInPackage(this.project,  this.domainModule, this.domainEventPkg.getText(), this.domainEventClass.getText(), domainContent);
        }

        // 创建 CommandApplication
        if (createCommandApplication.isSelected()){
            ApplicationTemplate.CreateCommandApplicationContext context = new ApplicationTemplate.CreateCommandApplicationContext(this.applicationPkg.getText(), this.commandApplication.getText());
            bindCommon(context);
            context.setCommandRepositoryTypeFull(this.repositoryPkg.getText() + "." + this.commandRepository.getText());
            String content = ApplicationTemplate.createCommandApplication(context);
            JavaFileCreator.createJavaFileInPackage(this.project, this.appModule, this.applicationPkg.getText(), this.commandApplication.getText(), content);
        }

        // 创建 CommandRepository
        if (createCommandRepository.isSelected()){
            RepositoryTemplate.CreateCommandRepositoryContext context = new RepositoryTemplate.CreateCommandRepositoryContext(this.repositoryPkg.getText(), this.commandRepository.getText());
            bindCommon(context);
            String commandContent = RepositoryTemplate.createCommand(context);
            JavaFileCreator.createJavaFileInPackage(this.project, this.domainModule,  this.repositoryPkg.getText(), this.commandRepository.getText(), commandContent);
        }

        // 创建 JpaBasedCommandRepository
        if (this.jpaCmdRepositoryEnable.isSelected()){
            JpaRepositoryTemplate.CreateJpaCommandRepositoryContext context = new JpaRepositoryTemplate
                    .CreateJpaCommandRepositoryContext(this.jpaRepositoryPkg.getText(), this.jpaCmdRepositoryImplClassName.getText());
            bindCommon(context);
            context.setRepositoryType(this.repositoryPkg.getText(), this.commandRepository.getText());
            String content = JpaRepositoryTemplate.createCommand(context);
            JavaFileCreator.createJavaFileInPackage(this.project, this.infraModule,
                    this.jpaRepositoryPkg.getText(),
                    this.jpaCmdRepositoryImplClassName.getText(),
                    content);
        }


        // 创建 View
        {
            ViewTemplate.CreateViewContext context =
                    new ViewTemplate.CreateViewContext(this.viewPkg.getText(), this.viewClassName.getText());
            bindCommon(context);
            context.setParentClassFull(viewParentClass.getText());
            context.setTableName(Utils.createTableByEntity(this.aggClassName.getText()));
            String content = ViewTemplate.create(context);
            JavaFileCreator.createJavaFileInPackage(this.project, this.domainModule,
                    this.viewPkg.getText(), this.viewClassName.getText(), content);
        }



        // 创建 QueryRepository
        if (createQueryRepository.isSelected()){
            RepositoryTemplate.CreateQueryRepositoryContext context = new RepositoryTemplate
                    .CreateQueryRepositoryContext(this.queryRepositoryPkg.getText(), this.queryRepository.getText());
            bindCommon(context);
            context.setViewTypeFull(this.viewPkg.getText() + "." + this.viewClassName.getText());
            context.setViewIdTypeFull(this.aggIdTextField.getText());
            String queryContent = RepositoryTemplate.createQuery(context);
            JavaFileCreator.createJavaFileInPackage(this.project, this.domainModule,
                    this.queryRepositoryPkg.getText(), this.queryRepository.getText(), queryContent);
        }

        // 创建 QueryApplication
        if (createQueryApplication.isSelected()){
            ApplicationTemplate.CreateQueryApplicationContext context = new ApplicationTemplate
                    .CreateQueryApplicationContext(this.queryApplicationPkg.getText(), this.queryApplication.getText());
            bindCommon(context);
            context.setViewTypeFull(this.viewPkg.getText() + "." + this.viewClassName.getText());
            context.setQueryRepositoryTypeFull(this.queryRepositoryPkg.getText() + "." + this.queryRepository.getText());
            String content = ApplicationTemplate.createQueryApplication(context);
            JavaFileCreator.createJavaFileInPackage(this.project, this.appModule,
                    this.queryApplicationPkg.getText(), this.queryApplication.getText(), content);
        }


        // 创建 JpaBasedQueryRepository
        if (this.jpaQueryRepositoryEnable.isSelected()){
            JpaRepositoryTemplate.CreateJpaQueryRepositoryContext context = new JpaRepositoryTemplate
                    .CreateJpaQueryRepositoryContext(
                            this.jpaQueryRepositoryPkg.getText(),
                            this.jpaQueryRepositoryImplClassName.getText());
            bindCommon(context);

            context.setRepositoryType(this.queryRepositoryPkg.getText(), this.queryRepository.getText());
            context.setViewTypeFull(this.viewPkg.getText() + "." + this.viewClassName.getText());
            context.setViewIdTypeFull(this.aggIdTextField.getText());
            String content = JpaRepositoryTemplate.createQuery(context);

            JavaFileCreator.createJavaFileInPackage(this.project, this.infraModule,
                    this.jpaQueryRepositoryPkg.getText(),
                    this.jpaQueryRepositoryImplClassName.getText(),
                    content);
        }

    }

    private void bindCommon(CreateClassContext context){
        context.setIdTypeFull(this.aggIdTextField.getText());
        context.setAggTypeFull(this.aggPackage.getText() + "." + this.aggClassName.getText());
    }

    @Override
    protected @Nullable JComponent createCenterPanel() {
        return contentPane;
    }
}
