package com.geekhalo.lego.plugin.resultconverter;

import com.geekhalo.lego.plugin.support.action.BaseAnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.vfs.VirtualFile;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

public class CreateResultConverterAction extends BaseAnAction {
    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        String pkg = getPackage(e);
        if (StringUtils.isEmpty(pkg)){
            Messages.showWarningDialog("选择正确的包名", "选择包");
            return;
        }
        VirtualFile packageAsFile = getPackageAsFile(e);

        VirtualFile parent = packageAsFile.getParent();
        Project project = e.getProject();

        String appType = findAggFull(project, parent);
        VirtualFile[] filesInPackage = getFilesInPackage(e);
        String cxtType = findFullType(filesInPackage, name -> name.endsWith("Context"));

        Module currentModule = findCurrentModule(e);
        CreateResultConverterDialog dialog = new CreateResultConverterDialog(e.getProject(), currentModule, pkg, cxtType, appType);
        dialog.show();
    }
}
