package com.geekhalo.user.domain.basic.create;

public interface PhoneNumberClient {
    void validate(String phoneNumber);
}
