package com.geekhalo.user.domain.basic;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class BasicUserKey {
    @NotNull
    @NotEmpty
    private String email;
}
