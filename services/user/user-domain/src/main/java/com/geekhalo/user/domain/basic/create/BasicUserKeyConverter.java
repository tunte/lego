package com.geekhalo.user.domain.basic.create;

import com.geekhalo.lego.core.command.support.handler.converter.AbstractSmartResultConverter;
import org.springframework.stereotype.Component;
import lombok.extern.slf4j.Slf4j;
import com.geekhalo.user.domain.basic.create.CreateBasicUserContext;
import com.geekhalo.user.domain.basic.BasicUser;
import com.geekhalo.user.domain.basic.BasicUserKey;

@Component
@Slf4j
public class BasicUserKeyConverter
        extends AbstractSmartResultConverter<BasicUser, CreateBasicUserContext, BasicUserKey> {

    public BasicUserKeyConverter(){
        super(BasicUser.class, CreateBasicUserContext.class, BasicUserKey.class);
    }

    @Override
    public BasicUserKey convert(BasicUser agg, CreateBasicUserContext context) {
        // 添加转换代码
        return null;
    }
}