package com.geekhalo.user.domain.basic.sync;

import lombok.Value;
import com.geekhalo.user.domain.basic.AbstractBasicUserEvent;
import com.geekhalo.user.domain.basic.BasicUser;

@Value
public class BasicUserSyncedEvent
        extends AbstractBasicUserEvent{
    public BasicUserSyncedEvent(BasicUser agg){
        super(agg);
    }
}
