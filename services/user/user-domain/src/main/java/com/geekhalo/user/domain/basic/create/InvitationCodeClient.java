package com.geekhalo.user.domain.basic.create;

public interface InvitationCodeClient {
    void lock(String code);
}
