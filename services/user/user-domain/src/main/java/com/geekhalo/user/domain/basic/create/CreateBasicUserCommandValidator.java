package com.geekhalo.user.domain.basic.create;

import com.geekhalo.lego.common.validator.ValidateErrorHandler;
import com.geekhalo.lego.core.validator.AbstractParamValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class CreateBasicUserCommandValidator
    extends AbstractParamValidator<CreateBasicUserCommand> {

    public CreateBasicUserCommandValidator(){
        super(CreateBasicUserCommand.class);
    }

    @Override
    public void validate(CreateBasicUserCommand cmd, ValidateErrorHandler validateErrorHandler) {
        log.info("validate(cmd) {}", cmd);
    }
}
