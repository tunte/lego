package com.geekhalo.user.domain.basic.updatePasswordByKey;


import com.geekhalo.lego.common.validator.ValidateErrorHandler;
import com.geekhalo.lego.core.command.CommandForUpdateByKey;
import com.geekhalo.user.domain.basic.BasicUserKey;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Data
@NoArgsConstructor
public class UpdatePasswordByKeyCommand implements CommandForUpdateByKey<BasicUserKey> {

    @NotNull
    private BasicUserKey key;

    @NotBlank(message = "当前密码不能为空")
    private String oldPassword;

    @NotBlank(message = "新密码不能为空")
    private String newPassword;

    @NotBlank(message = "确认密码不能为空")
    private String confirmPassword;

    // 实现验证方法
    @Override
    public void validate(ValidateErrorHandler errorHandler) {
        if (!Objects.equals(newPassword, confirmPassword)) {
            errorHandler.handleError("confirmPassword", "password_mismatch", "两次输入的新密码不一致");
        }

        // 其他与电子邮件和旧密码相关的验证逻辑...
    }

    public UpdatePasswordByKeyCommand(BasicUserKey key) {
        this.key = key;
     }

    @Override
    public  BasicUserKey getKey(){
        return this.key;
    }

    public static UpdatePasswordByKeyCommand apply(BasicUserKey key){
        UpdatePasswordByKeyCommand command = new UpdatePasswordByKeyCommand(key); 
        return command;
    }
}
