package com.geekhalo.user.domain.basic.sync;

import com.geekhalo.user.domain.basic.sync.SyncBasicUserCommand; 
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class SyncBasicUserContext{
    private SyncBasicUserCommand command;

    private SyncBasicUserContext(SyncBasicUserCommand command){
         this.command = command;
    }

    public static SyncBasicUserContext apply(SyncBasicUserCommand command) {
        SyncBasicUserContext context = new SyncBasicUserContext(command);
        return context;
    }
}
