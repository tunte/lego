package com.geekhalo.user.domain.basic;


import com.geekhalo.lego.core.query.QueryRepository;
import com.geekhalo.user.domain.basic.BasicUserView;
import java.lang.Long;

import java.util.Optional;

public interface BasicUserQueryRepository extends QueryRepository<BasicUserView, Long> {



}
