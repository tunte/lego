package com.geekhalo.user.domain.basic.create;

import com.geekhalo.lego.core.command.support.handler.converter.AbstractSmartResultConverter;
import org.springframework.stereotype.Component;
import lombok.extern.slf4j.Slf4j;
import com.geekhalo.user.domain.basic.BasicUser;
import com.geekhalo.user.domain.basic.BasicUserKey;

@Component
@Slf4j
public class BasicUserKeyConverter2
        extends AbstractSmartResultConverter<BasicUser, CreateBasicUserContext, BasicUserKey> {

    public BasicUserKeyConverter2(){
        super(BasicUser.class, CreateBasicUserContext.class, BasicUserKey.class);
    }

    @Override
    public BasicUserKey convert(BasicUser agg, CreateBasicUserContext context) {
        BasicUserKey key = new BasicUserKey();
        key.setEmail(agg.getEmail());
        return key;
    }
}