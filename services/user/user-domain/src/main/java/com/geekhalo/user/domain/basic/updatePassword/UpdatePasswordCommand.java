package com.geekhalo.user.domain.basic.updatePassword;

import com.geekhalo.lego.common.validator.ValidateErrorHandler;
import com.geekhalo.lego.core.command.CommandForUpdateById;
import java.lang.Long;
import java.util.Objects;

import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class UpdatePasswordCommand implements CommandForUpdateById<Long> {
    @NotNull(message = "用户ID不能为空")
    private Long id;

    @NotBlank(message = "当前密码不能为空")
    private String oldPassword;

    @NotBlank(message = "新密码不能为空")
    private String newPassword;

    // 确认密码字段（假设存在）
    @NotBlank(message = "确认密码不能为空")
    private String confirmPassword;

    public UpdatePasswordCommand(Long id){
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    // 根据Lego框架设计规范，实现验证方法
    @Override
    public void validate(ValidateErrorHandler errorHandler) {
        if (!Objects.equals(newPassword, confirmPassword)) {
            errorHandler.handleError("confirmPassword", "password_mismatch", "两次输入的新密码不一致");
        }

        // 其他验证逻辑...
    }


    public static UpdatePasswordCommand apply(Long id){
        UpdatePasswordCommand command = new UpdatePasswordCommand(id);
        return command;
    }
}
