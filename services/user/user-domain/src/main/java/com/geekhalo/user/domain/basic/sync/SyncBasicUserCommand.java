package com.geekhalo.user.domain.basic.sync;


import com.geekhalo.lego.common.validator.ValidateErrorHandler;
import com.geekhalo.lego.core.command.CommandForUpdateByKey;
import com.geekhalo.user.domain.basic.BasicUserKey;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.Objects;

@Data
@NoArgsConstructor
public class SyncBasicUserCommand implements CommandForUpdateByKey<BasicUserKey> {
    @NotNull
    private BasicUserKey key;
    // 手机号，要求格式正确且非空
    @NotBlank(message = "手机号不能为空")
    @Pattern(regexp = "^1[3-9]\\d{9}$", message = "手机号码格式不正确")
    private String phoneNumber;

    // 昵称，要求非空
    @NotBlank(message = "昵称不能为空")
    @Size(min = 2, max = 20, message = "昵称长度需在2到20个字符之间")
    private String nickname;

    // 生日，要求非空
    @NotNull(message = "生日不能为空")
    private LocalDate birthday;

    @NotBlank(message = "密码不能为空")
    @Size(min = 6, max = 20, message = "密码长度需在6到20个字符之间")
    private String password;

    @NotBlank(message = "确认密码不能为空")
    @Size(min = 6, max = 20, message = "确认密码长度需在6到20个字符之间")
    private String confirmPassword;

    public SyncBasicUserCommand(BasicUserKey key) {
        this.key = key;
    }

    // 根据Lego框架设计规范，实现验证方法
    @Override
    public void validate(ValidateErrorHandler errorHandler) {
        // 这里可以添加JSR-303/JSR-349注解验证或者其他业务逻辑相关的验证
        if (!Objects.equals(password, confirmPassword)) {
            errorHandler.handleError("confirmPassword", "password_mismatch", "两次输入的密码不一致");
        }
    }

    @Override
    public  BasicUserKey getKey(){
        return this.key;
    }

    public static SyncBasicUserCommand apply(BasicUserKey key){
        SyncBasicUserCommand command = new SyncBasicUserCommand(key); 
        return command;
    }
}
