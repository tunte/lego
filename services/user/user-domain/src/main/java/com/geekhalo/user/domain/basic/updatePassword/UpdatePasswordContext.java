package com.geekhalo.user.domain.basic.updatePassword;

import com.geekhalo.user.domain.basic.updatePassword.UpdatePasswordCommand; 
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class UpdatePasswordContext{
    private UpdatePasswordCommand command;

    private UpdatePasswordContext(UpdatePasswordCommand command){
         this.command = command;
    }

    public static UpdatePasswordContext apply(UpdatePasswordCommand command) {
        UpdatePasswordContext context = new UpdatePasswordContext(command);
        return context;
    }
}
