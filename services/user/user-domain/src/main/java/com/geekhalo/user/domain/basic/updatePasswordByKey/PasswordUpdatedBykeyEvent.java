package com.geekhalo.user.domain.basic.updatePasswordByKey;

import lombok.Value;
import com.geekhalo.user.domain.basic.AbstractBasicUserEvent;
import com.geekhalo.user.domain.basic.BasicUser;

@Value
public class PasswordUpdatedBykeyEvent
        extends AbstractBasicUserEvent{
    public PasswordUpdatedBykeyEvent(BasicUser agg){
        super(agg);
    }
}
