package com.geekhalo.user.domain.basic.create;

import com.geekhalo.lego.core.command.support.handler.aggfactory.AbstractSmartAggFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import lombok.extern.slf4j.Slf4j;
import com.geekhalo.user.domain.basic.create.CreateBasicUserContext;
import com.geekhalo.user.domain.basic.BasicUser;

@Component
@Slf4j
public class BasicUserFactory
        extends AbstractSmartAggFactory<CreateBasicUserContext, BasicUser> {
    /**
     * 依赖外部资源
     */
    @Autowired
    private SaltClient saltClient;

    public BasicUserFactory(){
        super(CreateBasicUserContext.class, BasicUser.class);
    }

    @Override
    public BasicUser create(CreateBasicUserContext context) {
        String salt = this.saltClient.create();
        return BasicUser.create(context, salt);
    }
}