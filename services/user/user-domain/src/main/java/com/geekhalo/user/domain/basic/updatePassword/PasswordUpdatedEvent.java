package com.geekhalo.user.domain.basic.updatePassword;

import lombok.Value;
import com.geekhalo.user.domain.basic.AbstractBasicUserEvent;
import com.geekhalo.user.domain.basic.BasicUser;

@Value
public class PasswordUpdatedEvent
        extends AbstractBasicUserEvent{
    public PasswordUpdatedEvent(BasicUser agg){
        super(agg);
    }
}
