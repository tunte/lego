package com.geekhalo.user.domain.basic.create;

import lombok.Value;
import com.geekhalo.user.domain.basic.AbstractBasicUserEvent;
import com.geekhalo.user.domain.basic.BasicUser;

@Value
public class BasicUserCreatedEvent
        extends AbstractBasicUserEvent{
    public BasicUserCreatedEvent(BasicUser agg){
        super(agg);
    }
}
