package com.geekhalo.user.domain.basic.create;

public interface UniqueIdClient {
    String next();
}
