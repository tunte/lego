package com.geekhalo.user.domain.basic;

import com.geekhalo.lego.common.enums.CommonEnum;

public enum UserStatus implements CommonEnum {
    ;
    private final int code;
    private final String descr;

    UserStatus(int code, String descr){
        this.code = code;
        this.descr = descr;
    }
    @Override
    public int getCode() {
        return this.code;
    }

    @Override
    public String getDescription() {
        return this.descr;
    }
}
