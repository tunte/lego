package com.geekhalo.user.domain.basic.updatePasswordByKey;

import com.geekhalo.user.domain.basic.updatePasswordByKey.UpdatePasswordByKeyCommand; 
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class UpdatePasswordByKeyContext{
    private UpdatePasswordByKeyCommand command;

    private UpdatePasswordByKeyContext(UpdatePasswordByKeyCommand command){
         this.command = command;
    }

    public static UpdatePasswordByKeyContext apply(UpdatePasswordByKeyCommand command) {
        UpdatePasswordByKeyContext context = new UpdatePasswordByKeyContext(command);
        return context;
    }
}
