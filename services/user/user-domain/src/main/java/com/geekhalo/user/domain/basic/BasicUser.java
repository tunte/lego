package com.geekhalo.user.domain.basic;

import com.geekhalo.lego.core.command.support.AbstractAggRoot;
import com.geekhalo.user.domain.basic.create.BasicUserCreatedEvent;
import com.geekhalo.user.domain.basic.create.CreateBasicUserCommand;
import com.geekhalo.user.domain.basic.create.CreateBasicUserContext;
import com.geekhalo.user.domain.basic.create.CreateBasicUserContext;
import com.geekhalo.user.domain.basic.sync.BasicUserSyncedEvent;
import com.geekhalo.user.domain.basic.sync.SyncBasicUserCommand;
import com.geekhalo.user.domain.basic.sync.SyncBasicUserContext;
import com.geekhalo.user.domain.basic.sync.SyncBasicUserContext;
import com.geekhalo.user.domain.basic.updatePassword.PasswordUpdatedEvent;
import com.geekhalo.user.domain.basic.updatePassword.UpdatePasswordCommand;
import com.geekhalo.user.domain.basic.updatePassword.UpdatePasswordContext;
import com.geekhalo.user.domain.basic.updatePassword.UpdatePasswordContext;
import com.geekhalo.user.domain.basic.updatePasswordByKey.PasswordUpdatedBykeyEvent;
import com.geekhalo.user.domain.basic.updatePasswordByKey.UpdatePasswordByKeyCommand;
import com.geekhalo.user.domain.basic.updatePasswordByKey.UpdatePasswordByKeyContext;
import com.geekhalo.user.domain.basic.updatePasswordByKey.UpdatePasswordByKeyContext;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "basic_user")
@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Setter(AccessLevel.PRIVATE)
public class BasicUser extends AbstractAggRoot { 
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String salt;

    private String email;


    public static BasicUser create(CreateBasicUserContext context, String salt) {
        BasicUser agg = new BasicUser();
        agg.setSalt(salt);
        // 添加代码
        agg.init();
        return agg;
    }

    private void init() {
        //添加代码
        addEvent(new BasicUserCreatedEvent(this));
    }

    public void updatePassword(UpdatePasswordContext context) {
        //添加代码
        addEvent(new PasswordUpdatedEvent(this));
    }

    public void updatePasswordByKey(UpdatePasswordByKeyContext context) {
        //添加代码
        addEvent(new PasswordUpdatedBykeyEvent(this));
    }

    public void sync(SyncBasicUserContext context) {
        //添加代码
        addEvent(new BasicUserSyncedEvent(this));
    }
}