package com.geekhalo.user.domain.basic;


import com.geekhalo.lego.core.command.CommandRepository;
import com.geekhalo.lego.core.command.CommandWithKeyRepository;
import com.geekhalo.user.domain.basic.BasicUser;
import java.lang.Long;

import java.util.Optional;

public interface BasicUserCommandRepository extends CommandRepository<BasicUser, Long>,
        CommandWithKeyRepository<BasicUser, Long, BasicUserKey> {
}
