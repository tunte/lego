package com.geekhalo.user.domain.basic;

import com.geekhalo.lego.core.query.AbstractView;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Getter;

import javax.persistence.*;

@Entity
@Table(name = "basic_user")
@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Setter(AccessLevel.PRIVATE)
public class BasicUserView extends AbstractView { 
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


}