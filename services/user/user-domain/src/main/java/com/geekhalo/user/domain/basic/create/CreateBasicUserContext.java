package com.geekhalo.user.domain.basic.create;

import com.geekhalo.user.domain.basic.create.CreateBasicUserCommand; 
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class CreateBasicUserContext{
    private CreateBasicUserCommand command;
    private String uniqueId;

    private CreateBasicUserContext(CreateBasicUserCommand command){
         this.command = command;
    }

    public static CreateBasicUserContext apply(CreateBasicUserCommand command) {
        CreateBasicUserContext context = new CreateBasicUserContext(command);
        return context;
    }
}
