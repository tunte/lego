//package com.geekhalo.user.domain.basic.create;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//@Component
//@Slf4j
//public class CreateBasicUserLockInvitationCode implements SmartPreOperation<CreateBasicUserContext>{
//    @Autowired
//    private InvitationCodeClient invitationCodeClient;
//
//    @Override
//    public boolean support(Class cls) {
//        return CreateBasicUserContext.class == cls;
//    }
//
//    @Override
//    public void action(CreateBasicUserContext context) {
//        this.invitationCodeClient.lock(context.getCommand().getCode());
//    }
//}
