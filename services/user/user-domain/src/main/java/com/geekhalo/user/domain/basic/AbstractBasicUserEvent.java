package com.geekhalo.user.domain.basic;

import com.geekhalo.lego.core.command.support.AbstractDomainEvent;
import java.lang.Long;

public abstract class AbstractBasicUserEvent
        extends AbstractDomainEvent<Long, BasicUser> {
    public AbstractBasicUserEvent(BasicUser agg) {
        super(agg);
    }
}
