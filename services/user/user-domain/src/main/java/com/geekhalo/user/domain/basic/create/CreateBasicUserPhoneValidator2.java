package com.geekhalo.user.domain.basic.create;

import com.geekhalo.lego.common.validator.ValidateErrorHandler;
import com.geekhalo.lego.core.validator.AbstractBusinessValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class CreateBasicUserPhoneValidator2
    extends AbstractBusinessValidator<CreateBasicUserContext> {
    @Autowired
    private PhoneNumberClient phoneNumberClient;
    public CreateBasicUserPhoneValidator2(){
        super(CreateBasicUserContext.class);
    }

    @Override
    public void validate(CreateBasicUserContext context, ValidateErrorHandler validateErrorHandler) {
        log.info("validate(context) {}", context);
        this.phoneNumberClient.validate(context.getCommand().getPhoneNumber());
    }
}
