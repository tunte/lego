package com.geekhalo.user.domain.basic.create;

import com.geekhalo.lego.core.command.support.handler.contextfactory.AbstractSmartContextFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import lombok.extern.slf4j.Slf4j;
import com.geekhalo.user.domain.basic.create.CreateBasicUserCommand;
import com.geekhalo.user.domain.basic.create.CreateBasicUserContext;

@Component
@Slf4j
public class CreateBasicUserContextFactory
        extends AbstractSmartContextFactory<CreateBasicUserCommand, CreateBasicUserContext> {
    @Autowired
    private UniqueIdClient uniqueIdClient;

    public CreateBasicUserContextFactory(){
        super(CreateBasicUserCommand.class, CreateBasicUserContext.class);
    }

    @Override
    public CreateBasicUserContext create(CreateBasicUserCommand cmd) {
        CreateBasicUserContext context = CreateBasicUserContext.apply(cmd);
        context.setUniqueId(this.uniqueIdClient.next());
        return context;
    }
}