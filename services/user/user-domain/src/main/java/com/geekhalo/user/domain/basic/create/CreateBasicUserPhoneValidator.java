package com.geekhalo.user.domain.basic.create;

import com.geekhalo.lego.common.validator.ValidateErrorHandler;
import com.geekhalo.lego.core.validator.AbstractBusinessValidator;
import com.geekhalo.user.domain.basic.create.CreateBasicUserContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class CreateBasicUserPhoneValidator
    extends AbstractBusinessValidator<CreateBasicUserContext> {

    public CreateBasicUserPhoneValidator(){
        super(CreateBasicUserContext.class);
    }

    @Override
    public void validate(CreateBasicUserContext context, ValidateErrorHandler validateErrorHandler) {
        log.info("validate(context) {}", context);
    }
}
