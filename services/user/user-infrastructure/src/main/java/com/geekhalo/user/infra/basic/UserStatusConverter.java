package com.geekhalo.user.infra.basic;

import com.geekhalo.lego.core.enums.repository.jpa.CommonEnumAttributeConverter;
import javax.persistence.Converter;
import com.geekhalo.user.domain.basic.UserStatus;

@Converter(autoApply = true)
public class UserStatusConverter 
       extends CommonEnumAttributeConverter<UserStatus> {
   public UserStatusConverter(){ 
       super(UserStatus.values());
   }
}