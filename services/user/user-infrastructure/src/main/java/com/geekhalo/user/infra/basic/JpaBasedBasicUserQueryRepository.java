package com.geekhalo.user.infra.basic;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.geekhalo.user.domain.basic.BasicUserQueryRepository;
import com.geekhalo.user.domain.basic.BasicUserView;
import java.lang.Long;

@Repository("basicUserQueryRepository")
public interface JpaBasedBasicUserQueryRepository extends BasicUserQueryRepository, JpaRepository<BasicUserView, Long> {



}
