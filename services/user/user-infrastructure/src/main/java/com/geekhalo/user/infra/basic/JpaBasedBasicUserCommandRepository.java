package com.geekhalo.user.infra.basic;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.geekhalo.user.domain.basic.BasicUserCommandRepository;
import com.geekhalo.user.domain.basic.BasicUser;
import java.lang.Long;

@Repository("basicUserCommandRepository")
public interface JpaBasedBasicUserCommandRepository extends BasicUserCommandRepository,JpaRepository<BasicUser, Long> {
    @Override
    default BasicUser sync(BasicUser agg){
        return save(agg);
    }


}
