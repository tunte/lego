package com.geekhalo.user.app.basic.query;

import com.geekhalo.lego.annotation.singlequery.FieldEqualTo;
import com.geekhalo.lego.core.singlequery.Pageable;
import com.geekhalo.lego.core.singlequery.Sort;
import com.geekhalo.user.domain.basic.UserStatus;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class PageByStatus{
    @FieldEqualTo("status")
    private UserStatus status;

    private Pageable pageable;

    private Sort sort;
}
