package com.geekhalo.user.app.basic;

import com.geekhalo.lego.core.command.CommandMethodDefinition;
import com.geekhalo.lego.core.command.CommandServiceDefinition;
import com.geekhalo.user.domain.basic.create.CreateBasicUserCommand;
import com.geekhalo.user.domain.basic.create.CreateBasicUserContext;
import com.geekhalo.user.domain.basic.sync.SyncBasicUserCommand;
import com.geekhalo.user.domain.basic.sync.SyncBasicUserContext;
import com.geekhalo.user.domain.basic.updatePassword.UpdatePasswordCommand;
import com.geekhalo.user.domain.basic.updatePassword.UpdatePasswordContext;
import com.geekhalo.user.domain.basic.updatePasswordByKey.UpdatePasswordByKeyCommand;
import com.geekhalo.user.domain.basic.updatePasswordByKey.UpdatePasswordByKeyContext;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import com.geekhalo.user.domain.basic.BasicUserCommandRepository;
import com.geekhalo.user.domain.basic.BasicUser;

@CommandServiceDefinition(
        domainClass = BasicUser.class,
        repositoryClass = BasicUserCommandRepository.class
)
@Validated
public interface BasicUserCommandApplication {
    // 创建方法，返回 聚合根
    @CommandMethodDefinition(contextClass = CreateBasicUserContext.class)
    BasicUser create(@Valid CreateBasicUserCommand command);

    // 更新方法，返回 void
    @CommandMethodDefinition(contextClass = UpdatePasswordContext.class)
    void updatePassword(@Valid UpdatePasswordCommand command);

    @CommandMethodDefinition(contextClass = UpdatePasswordByKeyContext.class)
    void updatePasswordByKey(@Valid UpdatePasswordByKeyCommand command);

    // 同步方法，返回 聚合根
    @CommandMethodDefinition(contextClass = SyncBasicUserContext.class)
    BasicUser sync(@Valid SyncBasicUserCommand command);
}
