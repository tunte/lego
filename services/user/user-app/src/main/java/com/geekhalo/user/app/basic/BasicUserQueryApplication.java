package com.geekhalo.user.app.basic;

import com.geekhalo.lego.core.query.QueryServiceDefinition;
import com.geekhalo.user.app.basic.query.PageByStatus;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import com.geekhalo.lego.core.singlequery.Page;
import java.util.List;
import com.geekhalo.user.domain.basic.BasicUserQueryRepository;
import com.geekhalo.user.domain.basic.BasicUserView;

@QueryServiceDefinition(
        repositoryClass = BasicUserQueryRepository.class,
        domainClass = BasicUserView.class
)
@Validated
public interface BasicUserQueryApplication {

    Page<BasicUserView> pageOf(@Valid PageByStatus query);
}
