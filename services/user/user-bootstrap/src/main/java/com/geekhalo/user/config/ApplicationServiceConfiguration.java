package com.geekhalo.user.config;

import com.geekhalo.lego.core.command.EnableCommandService;
import com.geekhalo.lego.core.query.EnableQueryService;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableCommandService(basePackages = "com.geekhalo.user.app")
@EnableQueryService(basePackages = "com.geekhalo.user.app")
public class ApplicationServiceConfiguration {
}
