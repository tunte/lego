package com.geekhalo.info.app.info.video;

import com.geekhalo.info.app.info.video.query.PageByCategory;
import com.geekhalo.lego.annotation.web.AutoRegisterWebController;
import com.geekhalo.lego.core.query.QueryServiceDefinition;
import com.geekhalo.info.domain.info.video.VideoInfoQueryRepository;
import com.geekhalo.info.domain.info.video.VideoInfoView;
import com.geekhalo.lego.core.singlequery.Page;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

@QueryServiceDefinition(
        repositoryClass = VideoInfoQueryRepository.class,
        domainClass = VideoInfoView.class
)
@Validated
@AutoRegisterWebController(name = "VideoInfoQuery")
public interface VideoInfoQueryApplication {

    Page<VideoInfoView> pageOf(@Valid PageByCategory query);
}
