package com.geekhalo.info.app.category;

import com.geekhalo.lego.annotation.web.AutoRegisterWebController;
import com.geekhalo.lego.core.query.QueryServiceDefinition;
import com.geekhalo.info.domain.category.InfoCategoryQueryRepository;
import com.geekhalo.info.domain.category.InfoCategoryView;

@QueryServiceDefinition(
        repositoryClass = InfoCategoryQueryRepository.class,
        domainClass = InfoCategoryView.class
)
@AutoRegisterWebController(name = "InfoCategoryQuery")
public interface InfoCategoryQueryApplication {

    InfoCategoryView getById(Long id);
}
