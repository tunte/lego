package com.geekhalo.info.app.info.text;

import com.geekhalo.lego.annotation.web.AutoRegisterWebController;
import com.geekhalo.lego.core.query.QueryServiceDefinition;
import com.geekhalo.info.domain.info.text.TextInfoQueryRepository;
import com.geekhalo.info.domain.info.text.TextInfoView;
import com.geekhalo.lego.core.singlequery.Page;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

@QueryServiceDefinition(
        repositoryClass = TextInfoQueryRepository.class,
        domainClass = TextInfoView.class
)
@Validated
@AutoRegisterWebController(name = "TextInfoQuery")
public interface TextInfoQueryApplication {

    TextInfoView getById(Long id);

    Page<TextInfoView> pageOf(@Valid PageByCategoryId pageByCategoryId);
}
