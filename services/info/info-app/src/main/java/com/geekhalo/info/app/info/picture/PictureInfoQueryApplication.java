package com.geekhalo.info.app.info.picture;

import com.geekhalo.lego.annotation.web.AutoRegisterWebController;
import com.geekhalo.lego.core.query.QueryServiceDefinition;
import com.geekhalo.info.domain.info.picture.PictureInfoQueryRepository;
import com.geekhalo.info.domain.info.picture.PictureInfoView;
import com.geekhalo.lego.core.singlequery.Page;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@QueryServiceDefinition(
        repositoryClass = PictureInfoQueryRepository.class,
        domainClass = PictureInfoView.class
)
@Validated
@AutoRegisterWebController(name = "PictureInfoQuery")
public interface PictureInfoQueryApplication {
    PictureInfoView getById(@Valid @NotNull Long id);

    Page<PictureInfoView> pageOf(@Valid PageByCategory pageByCategory);
}
