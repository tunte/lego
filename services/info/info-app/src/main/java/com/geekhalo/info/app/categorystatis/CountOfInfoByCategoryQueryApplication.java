package com.geekhalo.info.app.categorystatis;

import com.geekhalo.lego.core.query.QueryServiceDefinition;
import com.geekhalo.lego.core.singlequery.Page;
import java.util.List;
import com.geekhalo.info.domain.categorystatis.CountOfInfoByCategoryQueryRepository;
import com.geekhalo.info.domain.categorystatis.CountOfInfoByCategoryView;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

@QueryServiceDefinition(
        repositoryClass = CountOfInfoByCategoryQueryRepository.class,
        domainClass = CountOfInfoByCategoryView.class
)
@Validated
public interface CountOfInfoByCategoryQueryApplication {

    CountOfInfoByCategoryView getByCategoryId(@Valid Long categoryId);
}
