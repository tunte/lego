package com.geekhalo.info.app.info.video;

import com.geekhalo.info.domain.info.video.create.CreateVideoInfoCommand;
import com.geekhalo.info.domain.info.video.create.CreateVideoInfoContext;
import com.geekhalo.info.domain.info.video.offline.OfflineVideoInfoCommand;
import com.geekhalo.info.domain.info.video.offline.OfflineVideoInfoContext;
import com.geekhalo.info.domain.info.video.online.OnlineVideoInfoCommand;
import com.geekhalo.info.domain.info.video.online.OnlineVideoInfoContext;
import com.geekhalo.info.domain.info.video.update.UpdateVideoInfoCommand;
import com.geekhalo.info.domain.info.video.update.UpdateVideoInfoContext;
import com.geekhalo.lego.annotation.web.AutoRegisterWebController;
import com.geekhalo.lego.core.command.CommandMethodDefinition;
import com.geekhalo.lego.core.command.CommandServiceDefinition;
import com.geekhalo.info.domain.info.video.VideoInfoCommandRepository;
import com.geekhalo.info.domain.info.video.VideoInfo;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Optional;

@CommandServiceDefinition(
        domainClass = VideoInfo.class,
        repositoryClass = VideoInfoCommandRepository.class
)
@Validated
@AutoRegisterWebController(name = "VideoInfoManager")
public interface VideoInfoCommandApplication {

    @CommandMethodDefinition(contextClass = CreateVideoInfoContext.class)
    VideoInfo create(@Valid CreateVideoInfoCommand command);

    @CommandMethodDefinition(contextClass = UpdateVideoInfoContext.class)
    void update(@Valid UpdateVideoInfoCommand command);

    @CommandMethodDefinition(contextClass = OfflineVideoInfoContext.class)
    void offline(@Valid OfflineVideoInfoCommand command);

    @CommandMethodDefinition(contextClass = OnlineVideoInfoContext.class)
    void online(@Valid OnlineVideoInfoCommand command);

    Optional<VideoInfo> findById(@Valid @NotNull Long id);
}
