package com.geekhalo.info.app.info.picture.image;

import com.geekhalo.lego.core.query.QueryServiceDefinition;
import com.geekhalo.info.domain.info.picture.image.PictureInfoImageQueryRepository;
import com.geekhalo.info.domain.info.picture.image.PictureInfoImageView;

@QueryServiceDefinition(
        repositoryClass = PictureInfoImageQueryRepository.class,
        domainClass = PictureInfoImageView.class
)
public interface PictureInfoImageQueryApplication {

}
