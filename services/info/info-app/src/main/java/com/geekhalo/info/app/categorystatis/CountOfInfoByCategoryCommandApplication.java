package com.geekhalo.info.app.categorystatis;

import com.geekhalo.info.domain.categorystatis.Incr.IncrCountOfInfoByCategoryCommand;
import com.geekhalo.info.domain.categorystatis.Incr.IncrCountOfInfoByCategoryContext;
import com.geekhalo.lego.core.command.CommandMethodDefinition;
import com.geekhalo.lego.core.command.CommandServiceDefinition;
import com.geekhalo.info.domain.categorystatis.CountOfInfoByCategoryCommandRepository;
import com.geekhalo.info.domain.categorystatis.CountOfInfoByCategory;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

@CommandServiceDefinition(
        domainClass = CountOfInfoByCategory.class,
        repositoryClass = CountOfInfoByCategoryCommandRepository.class
)
@Validated
public interface CountOfInfoByCategoryCommandApplication {

    @CommandMethodDefinition(contextClass = IncrCountOfInfoByCategoryContext.class)
    void incr(@Valid IncrCountOfInfoByCategoryCommand command);
}
