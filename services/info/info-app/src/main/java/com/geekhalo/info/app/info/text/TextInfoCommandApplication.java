package com.geekhalo.info.app.info.text;

import com.geekhalo.info.domain.info.text.create.CreateTextInfoCommand;
import com.geekhalo.info.domain.info.text.create.CreateTextInfoContext;
import com.geekhalo.info.domain.info.text.offline.OfflineTextInfoCommand;
import com.geekhalo.info.domain.info.text.offline.OfflineTextInfoContext;
import com.geekhalo.info.domain.info.text.online.OnlineTextInfoCommand;
import com.geekhalo.info.domain.info.text.online.OnlineTextInfoContext;
import com.geekhalo.info.domain.info.text.update.UpdateTextInfoCommand;
import com.geekhalo.info.domain.info.text.update.UpdateTextInfoContext;
import com.geekhalo.lego.annotation.web.AutoRegisterWebController;
import com.geekhalo.lego.core.command.CommandMethodDefinition;
import com.geekhalo.lego.core.command.CommandServiceDefinition;
import com.geekhalo.info.domain.info.text.TextInfoCommandRepository;
import com.geekhalo.info.domain.info.text.TextInfo;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

@CommandServiceDefinition(
        domainClass = TextInfo.class,
        repositoryClass = TextInfoCommandRepository.class
)
@Validated
@AutoRegisterWebController(name = "TextInfoManager")
public interface TextInfoCommandApplication {

    @CommandMethodDefinition(contextClass = CreateTextInfoContext.class)
    TextInfo create(@Valid CreateTextInfoCommand command);

    @CommandMethodDefinition(contextClass = UpdateTextInfoContext.class)
    void update(@Valid UpdateTextInfoCommand command);

    @CommandMethodDefinition(contextClass = OnlineTextInfoContext.class)
    void online(@Valid OnlineTextInfoCommand command);

    @CommandMethodDefinition(contextClass = OfflineTextInfoContext.class)
    void offline(@Valid OfflineTextInfoCommand command);
}
