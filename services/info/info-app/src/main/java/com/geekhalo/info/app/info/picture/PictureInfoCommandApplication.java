package com.geekhalo.info.app.info.picture;

import com.geekhalo.info.domain.info.picture.create.CreatePictureInfoCommand;
import com.geekhalo.info.domain.info.picture.create.CreatePictureInfoContext;
import com.geekhalo.info.domain.info.picture.offline.OfflinePictureInfoCommand;
import com.geekhalo.info.domain.info.picture.offline.OfflinePictureInfoContext;
import com.geekhalo.info.domain.info.picture.online.OnlinePictureInfoCommand;
import com.geekhalo.info.domain.info.picture.online.OnlinePictureInfoContext;
import com.geekhalo.info.domain.info.picture.update.UpdatePictureInfoCommand;
import com.geekhalo.info.domain.info.picture.update.UpdatePictureInfoContext;
import com.geekhalo.lego.annotation.web.AutoRegisterWebController;
import com.geekhalo.lego.core.command.CommandMethodDefinition;
import com.geekhalo.lego.core.command.CommandServiceDefinition;
import com.geekhalo.info.domain.info.picture.PictureInfoCommandRepository;
import com.geekhalo.info.domain.info.picture.PictureInfo;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Optional;

@CommandServiceDefinition(
        domainClass = PictureInfo.class,
        repositoryClass = PictureInfoCommandRepository.class
)
@Validated
@AutoRegisterWebController(name = "PictureInfoManager")
public interface PictureInfoCommandApplication {

    @CommandMethodDefinition(contextClass = CreatePictureInfoContext.class)
    PictureInfo create(@Valid CreatePictureInfoCommand command);

    Optional<PictureInfo> findById(@Valid @NotNull Long id);

    @CommandMethodDefinition(contextClass = OnlinePictureInfoContext.class)
    void online(@Valid OnlinePictureInfoCommand command);

    @CommandMethodDefinition(contextClass = OfflinePictureInfoContext.class)
    void offline(@Valid OfflinePictureInfoCommand command);

    @CommandMethodDefinition(contextClass = UpdatePictureInfoContext.class)
    void update(@Valid UpdatePictureInfoCommand command);
}
