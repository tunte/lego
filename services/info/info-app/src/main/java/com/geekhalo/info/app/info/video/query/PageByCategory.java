package com.geekhalo.info.app.info.video.query;

import com.geekhalo.info.domain.info.InfoStatus;
import com.geekhalo.lego.annotation.singlequery.FieldEqualTo;
import com.geekhalo.lego.core.singlequery.Pageable;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class PageByCategory{
    @FieldEqualTo("categoryId")
    private Long categoryId;
    @FieldEqualTo("categoryId")
    private InfoStatus status;

    private Pageable pageable;
}
