package com.geekhalo.info.app.category;

import com.geekhalo.info.domain.category.create.CreateInfoCategoryCommand;
import com.geekhalo.info.domain.category.create.CreateInfoCategoryContext;
import com.geekhalo.info.domain.category.disable.DisableInfoCategoryCommand;
import com.geekhalo.info.domain.category.disable.DisableInfoCategoryContext;
import com.geekhalo.info.domain.category.enable.EnableInfoCategoryCommand;
import com.geekhalo.info.domain.category.enable.EnableInfoCategoryContext;
import com.geekhalo.info.domain.category.update.UpdateInfoCategoryCommand;
import com.geekhalo.info.domain.category.update.UpdateInfoCategoryContext;
import com.geekhalo.lego.annotation.web.AutoRegisterWebController;
import com.geekhalo.lego.core.command.CommandMethodDefinition;
import com.geekhalo.lego.core.command.CommandServiceDefinition;
import com.geekhalo.info.domain.category.InfoCategoryCommandRepository;
import com.geekhalo.info.domain.category.InfoCategory;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

@CommandServiceDefinition(
        domainClass = InfoCategory.class,
        repositoryClass = InfoCategoryCommandRepository.class
)
@Validated
@AutoRegisterWebController(name = "InfoCategoryManager")
public interface InfoCategoryCommandApplication {

    @CommandMethodDefinition(contextClass = CreateInfoCategoryContext.class)
    InfoCategory create(@Valid CreateInfoCategoryCommand command);

    @CommandMethodDefinition(contextClass = EnableInfoCategoryContext.class)
    void enable(@Valid EnableInfoCategoryCommand command);

    @CommandMethodDefinition(contextClass = DisableInfoCategoryContext.class)
    void disable(@Valid DisableInfoCategoryCommand command);

    @CommandMethodDefinition(contextClass = UpdateInfoCategoryContext.class)
    void update(@Valid UpdateInfoCategoryCommand command);
}
