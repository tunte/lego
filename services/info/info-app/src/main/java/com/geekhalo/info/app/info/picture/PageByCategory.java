package com.geekhalo.info.app.info.picture;

import com.geekhalo.info.domain.info.InfoStatus;
import com.geekhalo.lego.annotation.singlequery.FieldEqualTo;
import com.geekhalo.lego.core.singlequery.Pageable;
import lombok.Data;

@Data
public class PageByCategory {
    @FieldEqualTo("categoryId")
    private Long categoryId;

    @FieldEqualTo("infoStatus")
    private InfoStatus infoStatus;

    private Pageable pageable;
}
