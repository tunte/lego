package com.geekhalo.info.domain.info.picture.image;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class RemovePictureInfoImageCommand {
    @NotNull
    private Long imageId;
}
