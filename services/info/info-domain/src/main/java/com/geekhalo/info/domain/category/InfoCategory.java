package com.geekhalo.info.domain.category;

import com.geekhalo.info.domain.category.create.CreateInfoCategoryCommand;
import com.geekhalo.info.domain.category.create.CreateInfoCategoryContext;
import com.geekhalo.info.domain.category.create.InfoCategoryCreatedEvent;
import com.geekhalo.info.domain.category.disable.DisableInfoCategoryCommand;
import com.geekhalo.info.domain.category.disable.DisableInfoCategoryContext;
import com.geekhalo.info.domain.category.disable.InfoCategoryDisabledEvent;
import com.geekhalo.info.domain.category.enable.EnableInfoCategoryCommand;
import com.geekhalo.info.domain.category.enable.EnableInfoCategoryContext;
import com.geekhalo.info.domain.category.enable.InfoCategoryEnabledEvent;
import com.geekhalo.info.domain.category.exception.CanNotCreateInfoException;
import com.geekhalo.info.domain.category.update.InfoCategoryUpdatedEvent;
import com.geekhalo.info.domain.category.update.UpdateInfoCategoryCommand;
import com.geekhalo.info.domain.category.update.UpdateInfoCategoryContext;
import com.geekhalo.lego.core.command.support.AbstractAggRoot;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;

@Entity
@Table(name = "info_category")
@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Setter(AccessLevel.PRIVATE)
public class InfoCategory extends AbstractAggRoot { 
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private Integer showOrder;

    private InfoCategoryStatus status;


    public static InfoCategory create(CreateInfoCategoryContext context) {
        CreateInfoCategoryCommand command = context.getCommand();

        // 添加代码
        InfoCategory infoCategory = new InfoCategory();
        infoCategory.setName(command.getName());
        infoCategory.setShowOrder(command.getShowOrder());
        infoCategory.init();
        return infoCategory;
    }

    private void init() {
        this.setStatus(InfoCategoryStatus.ENABLE);
        this.addEvent(new InfoCategoryCreatedEvent(this));
    }


    public void enable(EnableInfoCategoryContext context) {
        if (getStatus() == InfoCategoryStatus.ENABLE){
            return;
        }
        setStatus(InfoCategoryStatus.ENABLE);
        addEvent(new InfoCategoryEnabledEvent(this));
    }

    public void disable(DisableInfoCategoryContext context) {
        if (getStatus() == InfoCategoryStatus.DISABLE){
            return;
        }
        setStatus(InfoCategoryStatus.DISABLE);
        addEvent(new InfoCategoryDisabledEvent(this));
    }

    public void update(UpdateInfoCategoryContext context) {
        UpdateInfoCategoryCommand command = context.getCommand();
        if (StringUtils.isNotEmpty(command.getName())){
            setName(command.getName());
        }
        if (command.getShowOrder() != null){
            setShowOrder(command.getShowOrder());
        }
        addEvent(new InfoCategoryUpdatedEvent(this));
    }

    public void checkCanCreateInfo() {
        if (getStatus() != InfoCategoryStatus.ENABLE){
            throw new CanNotCreateInfoException();
        }
    }
}