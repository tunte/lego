package com.geekhalo.info.domain.info.picture.create;

import com.geekhalo.info.domain.info.picture.image.CreatePictureInfoImageCommand;
import com.geekhalo.lego.core.command.CommandForCreate;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreatePictureInfoCommand implements CommandForCreate {
    @NotNull
    private Long categoryId;

    @NotNull
    private String title;

    @NotEmpty
    private List<CreatePictureInfoImageCommand> images;

}
