package com.geekhalo.info.domain.categorystatis;

import com.geekhalo.info.domain.categorystatis.Incr.CountOfInfoByCategoryIncredEvent;
import com.geekhalo.info.domain.categorystatis.Incr.IncrCountOfInfoByCategoryCommand;
import com.geekhalo.info.domain.categorystatis.Incr.IncrCountOfInfoByCategoryContext;
import com.geekhalo.lego.core.command.support.AbstractAggRoot;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "count_of_info_by_category")
@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Setter(AccessLevel.PRIVATE)
public class CountOfInfoByCategory extends AbstractAggRoot { 
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long categoryId;

    private Integer count;


    public static CountOfInfoByCategory create(IncrCountOfInfoByCategoryContext context) {
        CountOfInfoByCategory category = new CountOfInfoByCategory();
        category.setCategoryId(context.getCommand().getKey());
        category.setCount(0);
        return category;
    }

    public void incr(IncrCountOfInfoByCategoryContext context){
        count += context.getCommand().getCount();
    }
}