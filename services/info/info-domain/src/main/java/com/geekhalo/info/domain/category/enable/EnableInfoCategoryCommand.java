package com.geekhalo.info.domain.category.enable;

import com.geekhalo.lego.core.command.CommandForUpdateById;
import java.lang.Long;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class EnableInfoCategoryCommand implements CommandForUpdateById<Long> {
    @NotNull
    private Long id;

    public EnableInfoCategoryCommand(Long id){
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public static EnableInfoCategoryCommand apply(Long id){
        EnableInfoCategoryCommand command = new EnableInfoCategoryCommand(id); 
        return command;
    }
}
