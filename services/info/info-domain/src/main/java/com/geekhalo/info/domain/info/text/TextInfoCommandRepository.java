package com.geekhalo.info.domain.info.text;


import com.geekhalo.lego.core.command.CommandRepository;
import com.geekhalo.info.domain.info.text.TextInfo;
import java.lang.Long;

import java.util.Optional;

public interface TextInfoCommandRepository extends CommandRepository<TextInfo, Long> {



}
