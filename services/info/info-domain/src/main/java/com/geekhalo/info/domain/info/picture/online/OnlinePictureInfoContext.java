package com.geekhalo.info.domain.info.picture.online;

import com.geekhalo.info.domain.info.picture.online.OnlinePictureInfoCommand; 
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class OnlinePictureInfoContext{
    private OnlinePictureInfoCommand command;

    private OnlinePictureInfoContext(OnlinePictureInfoCommand command){
         this.command = command;
    }

    public static OnlinePictureInfoContext apply(OnlinePictureInfoCommand command) {
        OnlinePictureInfoContext context = new OnlinePictureInfoContext(command);
        return context;
    }
}
