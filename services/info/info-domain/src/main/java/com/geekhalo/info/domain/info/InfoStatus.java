package com.geekhalo.info.domain.info;

import com.geekhalo.lego.common.enums.CommonEnum;

public enum InfoStatus implements CommonEnum {
    ONLINE(1, "上架"),
    OFFLINE(2, "下架");
    private final int code;
    private final String descr;

    InfoStatus(int code, String descr){
        this.code = code;
        this.descr = descr;
    }
    @Override
    public int getCode() {
        return this.code;
    }

    @Override
    public String getDescription() {
        return this.descr;
    }
}
