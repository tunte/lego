package com.geekhalo.info.domain.info.video.create;

import com.geekhalo.lego.core.command.CommandForCreate;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateVideoInfoCommand implements CommandForCreate {
    @NotNull
    private Long categoryId;
    @NotNull
    private String title;
    @NotNull
    private Long duration;
    @NotNull
    private String videoUrl;

}
