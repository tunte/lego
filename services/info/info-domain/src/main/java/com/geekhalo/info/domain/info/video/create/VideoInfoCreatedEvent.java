package com.geekhalo.info.domain.info.video.create;

import lombok.Value;
import com.geekhalo.info.domain.info.video.AbstractVideoInfoEvent;
import com.geekhalo.info.domain.info.video.VideoInfo;

@Value
public class VideoInfoCreatedEvent
        extends AbstractVideoInfoEvent{
    public VideoInfoCreatedEvent(VideoInfo agg){
        super(agg);
    }
}
