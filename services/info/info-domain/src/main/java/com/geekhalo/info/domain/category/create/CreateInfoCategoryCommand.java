package com.geekhalo.info.domain.category.create;

import com.geekhalo.lego.core.command.CommandForCreate;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateInfoCategoryCommand implements CommandForCreate {
    @NotEmpty
    private String name;
    @NotNull
    private Integer showOrder;

}
