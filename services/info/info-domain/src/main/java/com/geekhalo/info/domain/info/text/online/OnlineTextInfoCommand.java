package com.geekhalo.info.domain.info.text.online;

import com.geekhalo.lego.core.command.CommandForUpdateById;
import java.lang.Long;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class OnlineTextInfoCommand implements CommandForUpdateById<Long> {
    @NotNull
    private Long id;

    public OnlineTextInfoCommand(Long id){
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public static OnlineTextInfoCommand apply(Long id){
        OnlineTextInfoCommand command = new OnlineTextInfoCommand(id); 
        return command;
    }
}
