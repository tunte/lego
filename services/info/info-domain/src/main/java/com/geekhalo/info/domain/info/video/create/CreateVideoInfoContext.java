package com.geekhalo.info.domain.info.video.create;

import com.geekhalo.info.domain.category.InfoCategory;
import com.geekhalo.info.domain.category.LoadInfoCategoryById;
import com.geekhalo.info.domain.info.video.create.CreateVideoInfoCommand;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.Optional;

@NoArgsConstructor
@Data
public class CreateVideoInfoContext{
    private CreateVideoInfoCommand command;

    @LoadInfoCategoryById(id = "command.categoryId")
    private Optional<InfoCategory> category;

    private CreateVideoInfoContext(CreateVideoInfoCommand command){
         this.command = command;
    }

    public static CreateVideoInfoContext apply(CreateVideoInfoCommand command) {
        CreateVideoInfoContext context = new CreateVideoInfoContext(command);
        return context;
    }
}
