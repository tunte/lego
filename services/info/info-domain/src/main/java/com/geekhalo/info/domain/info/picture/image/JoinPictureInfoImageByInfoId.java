package com.geekhalo.info.domain.info.picture.image;

import com.geekhalo.lego.annotation.joininmemory.JoinInMemory;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@JoinInMemory(keyFromSourceData = "",
        keyFromJoinData = "#{infoId}",
        loader = "#{@pictureInfoImageQueryRepository.getByInfoIdIn(#root)}"
)
public @interface JoinPictureInfoImageByInfoId {
    @AliasFor(
            annotation = JoinInMemory.class
    )
    String keyFromSourceData();
}
