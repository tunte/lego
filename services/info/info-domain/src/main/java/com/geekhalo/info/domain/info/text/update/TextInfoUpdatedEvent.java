package com.geekhalo.info.domain.info.text.update;

import lombok.Value;
import com.geekhalo.info.domain.info.text.AbstractTextInfoEvent;
import com.geekhalo.info.domain.info.text.TextInfo;

@Value
public class TextInfoUpdatedEvent
        extends AbstractTextInfoEvent{
    public TextInfoUpdatedEvent(TextInfo agg){
        super(agg);
    }
}
