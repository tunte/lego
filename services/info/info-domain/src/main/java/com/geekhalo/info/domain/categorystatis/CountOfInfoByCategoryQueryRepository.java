package com.geekhalo.info.domain.categorystatis;


import com.geekhalo.lego.core.query.QueryRepository;
import com.geekhalo.info.domain.categorystatis.CountOfInfoByCategoryView;
import java.lang.Long;

import java.util.Optional;

public interface CountOfInfoByCategoryQueryRepository extends QueryRepository<CountOfInfoByCategoryView, Long> {



}
