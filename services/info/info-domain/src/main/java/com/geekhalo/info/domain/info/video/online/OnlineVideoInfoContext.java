package com.geekhalo.info.domain.info.video.online;

import com.geekhalo.info.domain.info.video.online.OnlineVideoInfoCommand; 
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class OnlineVideoInfoContext{
    private OnlineVideoInfoCommand command;

    private OnlineVideoInfoContext(OnlineVideoInfoCommand command){
         this.command = command;
    }

    public static OnlineVideoInfoContext apply(OnlineVideoInfoCommand command) {
        OnlineVideoInfoContext context = new OnlineVideoInfoContext(command);
        return context;
    }
}
