package com.geekhalo.info.domain.info.text.create;

import lombok.Value;
import com.geekhalo.info.domain.info.text.AbstractTextInfoEvent;
import com.geekhalo.info.domain.info.text.TextInfo;

@Value
public class TextInfoCreatedEvent
        extends AbstractTextInfoEvent{
    public TextInfoCreatedEvent(TextInfo agg){
        super(agg);
    }
}
