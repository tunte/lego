package com.geekhalo.info.domain.info.picture.create;

import com.geekhalo.info.domain.category.InfoCategory;
import com.geekhalo.info.domain.category.LoadInfoCategoryById;
import com.geekhalo.info.domain.info.picture.create.CreatePictureInfoCommand;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.Optional;

@NoArgsConstructor
@Data
public class CreatePictureInfoContext{
    private CreatePictureInfoCommand command;

    @LoadInfoCategoryById(id = "command.categoryId")
    private Optional<InfoCategory> category;

    private CreatePictureInfoContext(CreatePictureInfoCommand command){
         this.command = command;
    }

    public static CreatePictureInfoContext apply(CreatePictureInfoCommand command) {
        CreatePictureInfoContext context = new CreatePictureInfoContext(command);
        return context;
    }
}
