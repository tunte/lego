package com.geekhalo.info.domain.category;

import com.geekhalo.lego.annotation.loader.LazyLoadBy;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static com.geekhalo.info.domain.category.LoadInfoCategoryById.BEAN_NAME;

@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@LazyLoadBy("#{@"+ BEAN_NAME +".findById(${id})}")
public @interface LoadInfoCategoryById {
    String BEAN_NAME = "infoCategoryCommandRepository";
    String id();

}
