package com.geekhalo.info.domain.info.video;


import com.geekhalo.lego.core.command.CommandRepository;
import com.geekhalo.info.domain.info.video.VideoInfo;
import java.lang.Long;

import java.util.Optional;

public interface VideoInfoCommandRepository extends CommandRepository<VideoInfo, Long> {



}
