package com.geekhalo.info.domain.categorystatis.Incr;

import com.geekhalo.lego.core.command.CommandForSync;
import java.lang.Long;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;


@Data
@NoArgsConstructor
public class IncrCountOfInfoByCategoryCommand implements CommandForSync<Long> {
    @NotNull
    private Long key;
    @NotNull
    private Integer count;

     public IncrCountOfInfoByCategoryCommand(Long key) {
        this.key = key;
    }

    @Override
    public Long getKey() {
        return this.key;
    }

    public static IncrCountOfInfoByCategoryCommand apply(Long key){
        IncrCountOfInfoByCategoryCommand command = new IncrCountOfInfoByCategoryCommand(key); 
        return command;
    }
}
