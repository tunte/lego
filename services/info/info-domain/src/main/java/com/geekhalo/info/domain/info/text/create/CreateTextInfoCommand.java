package com.geekhalo.info.domain.info.text.create;

import com.geekhalo.lego.core.command.CommandForCreate;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateTextInfoCommand implements CommandForCreate {
    @NotNull
    private Long categoryId;
    @NotNull
    private String title;
    @NotNull
    private String detail;

}
