package com.geekhalo.info.domain.categorystatis;


import com.geekhalo.lego.core.command.CommandRepository;
import com.geekhalo.info.domain.categorystatis.CountOfInfoByCategory;
import com.geekhalo.lego.core.command.CommandWithKeyRepository;

import java.lang.Long;

import java.util.Optional;

public interface CountOfInfoByCategoryCommandRepository
        extends CommandWithKeyRepository<CountOfInfoByCategory, Long, Long> {

    @Override
    default Optional<CountOfInfoByCategory> findByKey(Long key){
        return getByCategoryId(key);
    }

    Optional<CountOfInfoByCategory> getByCategoryId(Long categoryId);

}
