package com.geekhalo.info.domain.info.picture.offline;

import com.geekhalo.info.domain.info.picture.offline.OfflinePictureInfoCommand; 
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class OfflinePictureInfoContext{
    private OfflinePictureInfoCommand command;

    private OfflinePictureInfoContext(OfflinePictureInfoCommand command){
         this.command = command;
    }

    public static OfflinePictureInfoContext apply(OfflinePictureInfoCommand command) {
        OfflinePictureInfoContext context = new OfflinePictureInfoContext(command);
        return context;
    }
}
