package com.geekhalo.info.domain.category.create;

import com.geekhalo.info.domain.category.create.CreateInfoCategoryCommand; 
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class CreateInfoCategoryContext{
    private CreateInfoCategoryCommand command;

    private CreateInfoCategoryContext(CreateInfoCategoryCommand command){
         this.command = command;
    }

    public static CreateInfoCategoryContext apply(CreateInfoCategoryCommand command) {
        CreateInfoCategoryContext context = new CreateInfoCategoryContext(command);
        return context;
    }
}
