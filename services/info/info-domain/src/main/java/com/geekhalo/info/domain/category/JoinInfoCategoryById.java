package com.geekhalo.info.domain.category;

import com.geekhalo.lego.annotation.joininmemory.JoinInMemory;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@JoinInMemory(keyFromSourceData = "",
        keyFromJoinData = "#{id}",
        loader = "#{@infoCategoryQueryRepository.getByIds(#root)}"
)
public @interface JoinInfoCategoryById {
    @AliasFor(
            annotation = JoinInMemory.class
    )
    String keyFromSourceData();
}
