package com.geekhalo.info.domain.info.picture.update;

import lombok.Value;
import com.geekhalo.info.domain.info.picture.AbstractPictureInfoEvent;
import com.geekhalo.info.domain.info.picture.PictureInfo;

@Value
public class PictureInfoUpdatedEvent
        extends AbstractPictureInfoEvent{
    public PictureInfoUpdatedEvent(PictureInfo agg){
        super(agg);
    }
}
