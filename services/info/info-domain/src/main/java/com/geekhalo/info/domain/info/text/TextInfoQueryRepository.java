package com.geekhalo.info.domain.info.text;


import com.geekhalo.lego.core.query.QueryRepository;
import com.geekhalo.info.domain.info.text.TextInfoView;
import java.lang.Long;

import java.util.Optional;

public interface TextInfoQueryRepository extends QueryRepository<TextInfoView, Long> {



}
