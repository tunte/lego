package com.geekhalo.info.domain.info.video.online;

import lombok.Value;
import com.geekhalo.info.domain.info.video.AbstractVideoInfoEvent;
import com.geekhalo.info.domain.info.video.VideoInfo;

@Value
public class VideoInfoOnlinedEvent
        extends AbstractVideoInfoEvent{
    public VideoInfoOnlinedEvent(VideoInfo agg){
        super(agg);
    }
}
