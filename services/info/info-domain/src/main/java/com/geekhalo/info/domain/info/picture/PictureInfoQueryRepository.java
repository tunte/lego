package com.geekhalo.info.domain.info.picture;


import com.geekhalo.lego.core.query.QueryRepository;
import com.geekhalo.info.domain.info.picture.PictureInfoView;
import java.lang.Long;

import java.util.Optional;

public interface PictureInfoQueryRepository extends QueryRepository<PictureInfoView, Long> {



}
