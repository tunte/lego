package com.geekhalo.info.domain.info.video.offline;

import com.geekhalo.info.domain.info.video.offline.OfflineVideoInfoCommand; 
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class OfflineVideoInfoContext{
    private OfflineVideoInfoCommand command;

    private OfflineVideoInfoContext(OfflineVideoInfoCommand command){
         this.command = command;
    }

    public static OfflineVideoInfoContext apply(OfflineVideoInfoCommand command) {
        OfflineVideoInfoContext context = new OfflineVideoInfoContext(command);
        return context;
    }
}
