package com.geekhalo.info.domain.category;

import com.geekhalo.lego.core.command.support.AbstractDomainEvent;
import java.lang.Long;

public abstract class AbstractInfoCategoryEvent
        extends AbstractDomainEvent<Long, InfoCategory> {
    public AbstractInfoCategoryEvent(InfoCategory agg) {
        super(agg);
    }
}
