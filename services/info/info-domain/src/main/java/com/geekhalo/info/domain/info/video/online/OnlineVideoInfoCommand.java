package com.geekhalo.info.domain.info.video.online;

import com.geekhalo.lego.core.command.CommandForUpdateById;
import java.lang.Long;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class OnlineVideoInfoCommand implements CommandForUpdateById<Long> {
    @NotNull
    private Long id;

    public OnlineVideoInfoCommand(Long id){
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public static OnlineVideoInfoCommand apply(Long id){
        OnlineVideoInfoCommand command = new OnlineVideoInfoCommand(id); 
        return command;
    }
}
