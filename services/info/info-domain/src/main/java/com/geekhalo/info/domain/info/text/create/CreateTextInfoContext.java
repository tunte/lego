package com.geekhalo.info.domain.info.text.create;

import com.geekhalo.info.domain.category.InfoCategory;
import com.geekhalo.info.domain.category.LoadInfoCategoryById;
import com.geekhalo.info.domain.info.text.create.CreateTextInfoCommand;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.Optional;

@NoArgsConstructor
@Data
public class CreateTextInfoContext{
    private CreateTextInfoCommand command;

    @LoadInfoCategoryById(id = "command.categoryId")
    private Optional<InfoCategory> category;

    private CreateTextInfoContext(CreateTextInfoCommand command){
         this.command = command;
    }

    public static CreateTextInfoContext apply(CreateTextInfoCommand command) {
        CreateTextInfoContext context = new CreateTextInfoContext(command);
        return context;
    }
}
