package com.geekhalo.info.domain.info.picture.image;


import com.geekhalo.lego.core.query.QueryRepository;
import com.geekhalo.info.domain.info.picture.image.PictureInfoImageView;
import java.lang.Long;

import java.util.List;
import java.util.Optional;

public interface PictureInfoImageQueryRepository extends QueryRepository<PictureInfoImageView, Long> {

    List<PictureInfoImageView> getByInfoIdIn(List<Long> infoId);

}
