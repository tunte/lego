package com.geekhalo.info.domain.category.update;

import com.geekhalo.lego.core.command.CommandForUpdateById;
import java.lang.Long;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class UpdateInfoCategoryCommand implements CommandForUpdateById<Long> {
    @NotNull
    private Long id;
    private String name;
    private Integer showOrder;

    public UpdateInfoCategoryCommand(Long id){
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public static UpdateInfoCategoryCommand apply(Long id){
        UpdateInfoCategoryCommand command = new UpdateInfoCategoryCommand(id); 
        return command;
    }
}
