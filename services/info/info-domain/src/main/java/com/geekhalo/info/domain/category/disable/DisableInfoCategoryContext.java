package com.geekhalo.info.domain.category.disable;

import com.geekhalo.info.domain.category.disable.DisableInfoCategoryCommand; 
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class DisableInfoCategoryContext{
    private DisableInfoCategoryCommand command;

    private DisableInfoCategoryContext(DisableInfoCategoryCommand command){
         this.command = command;
    }

    public static DisableInfoCategoryContext apply(DisableInfoCategoryCommand command) {
        DisableInfoCategoryContext context = new DisableInfoCategoryContext(command);
        return context;
    }
}
