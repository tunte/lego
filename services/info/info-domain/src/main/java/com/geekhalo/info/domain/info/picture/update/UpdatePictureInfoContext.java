package com.geekhalo.info.domain.info.picture.update;

import com.geekhalo.info.domain.info.picture.update.UpdatePictureInfoCommand; 
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class UpdatePictureInfoContext{
    private UpdatePictureInfoCommand command;

    private UpdatePictureInfoContext(UpdatePictureInfoCommand command){
         this.command = command;
    }

    public static UpdatePictureInfoContext apply(UpdatePictureInfoCommand command) {
        UpdatePictureInfoContext context = new UpdatePictureInfoContext(command);
        return context;
    }
}
