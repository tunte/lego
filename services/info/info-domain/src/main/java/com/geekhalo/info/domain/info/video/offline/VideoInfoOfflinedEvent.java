package com.geekhalo.info.domain.info.video.offline;

import lombok.Value;
import com.geekhalo.info.domain.info.video.AbstractVideoInfoEvent;
import com.geekhalo.info.domain.info.video.VideoInfo;

@Value
public class VideoInfoOfflinedEvent
        extends AbstractVideoInfoEvent{
    public VideoInfoOfflinedEvent(VideoInfo agg){
        super(agg);
    }
}
