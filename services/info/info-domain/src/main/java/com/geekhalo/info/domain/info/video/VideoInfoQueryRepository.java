package com.geekhalo.info.domain.info.video;


import com.geekhalo.lego.core.query.QueryRepository;
import com.geekhalo.info.domain.info.video.VideoInfoView;
import java.lang.Long;

import java.util.Optional;

public interface VideoInfoQueryRepository extends QueryRepository<VideoInfoView, Long> {



}
