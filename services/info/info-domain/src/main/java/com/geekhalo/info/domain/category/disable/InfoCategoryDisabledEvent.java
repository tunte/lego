package com.geekhalo.info.domain.category.disable;

import lombok.Value;
import com.geekhalo.info.domain.category.AbstractInfoCategoryEvent;
import com.geekhalo.info.domain.category.InfoCategory;

@Value
public class InfoCategoryDisabledEvent
        extends AbstractInfoCategoryEvent{
    public InfoCategoryDisabledEvent(InfoCategory agg){
        super(agg);
    }
}
