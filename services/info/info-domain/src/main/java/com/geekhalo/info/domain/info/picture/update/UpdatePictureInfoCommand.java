package com.geekhalo.info.domain.info.picture.update;

import com.geekhalo.info.domain.info.picture.image.CreatePictureInfoImageCommand;
import com.geekhalo.info.domain.info.picture.image.RemovePictureInfoImageCommand;
import com.geekhalo.info.domain.info.picture.image.UpdatePictureInfoImageCommand;
import com.geekhalo.lego.core.command.CommandForUpdateById;
import java.lang.Long;
import java.util.List;

import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class UpdatePictureInfoCommand implements CommandForUpdateById<Long> {
    @NotNull
    private Long id;

    private String title;

    private List<CreatePictureInfoImageCommand> createImageCommands;

    private List<UpdatePictureInfoImageCommand> updateImageCommands;

    private List<RemovePictureInfoImageCommand> removeImageCommands;

    public UpdatePictureInfoCommand(Long id){
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public static UpdatePictureInfoCommand apply(Long id){
        UpdatePictureInfoCommand command = new UpdatePictureInfoCommand(id); 
        return command;
    }
}
