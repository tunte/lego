package com.geekhalo.info.domain.info.picture.offline;

import com.geekhalo.lego.core.command.CommandForUpdateById;
import java.lang.Long;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class OfflinePictureInfoCommand implements CommandForUpdateById<Long> {
    @NotNull
    private Long id;

    public OfflinePictureInfoCommand(Long id){
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public static OfflinePictureInfoCommand apply(Long id){
        OfflinePictureInfoCommand command = new OfflinePictureInfoCommand(id); 
        return command;
    }
}
