package com.geekhalo.info.domain.category.update;

import com.geekhalo.info.domain.category.update.UpdateInfoCategoryCommand; 
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class UpdateInfoCategoryContext{
    private UpdateInfoCategoryCommand command;

    private UpdateInfoCategoryContext(UpdateInfoCategoryCommand command){
         this.command = command;
    }

    public static UpdateInfoCategoryContext apply(UpdateInfoCategoryCommand command) {
        UpdateInfoCategoryContext context = new UpdateInfoCategoryContext(command);
        return context;
    }
}
