package com.geekhalo.info.domain.categorystatis.Incr;

import com.geekhalo.info.domain.categorystatis.Incr.IncrCountOfInfoByCategoryCommand; 
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class IncrCountOfInfoByCategoryContext{
    private IncrCountOfInfoByCategoryCommand command;

    private IncrCountOfInfoByCategoryContext(IncrCountOfInfoByCategoryCommand command){
         this.command = command;
    }

    public static IncrCountOfInfoByCategoryContext apply(IncrCountOfInfoByCategoryCommand command) {
        IncrCountOfInfoByCategoryContext context = new IncrCountOfInfoByCategoryContext(command);
        return context;
    }
}
