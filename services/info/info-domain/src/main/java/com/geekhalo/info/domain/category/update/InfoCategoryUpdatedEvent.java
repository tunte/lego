package com.geekhalo.info.domain.category.update;

import lombok.Value;
import com.geekhalo.info.domain.category.AbstractInfoCategoryEvent;
import com.geekhalo.info.domain.category.InfoCategory;

@Value
public class InfoCategoryUpdatedEvent
        extends AbstractInfoCategoryEvent{
    public InfoCategoryUpdatedEvent(InfoCategory agg){
        super(agg);
    }
}
