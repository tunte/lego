package com.geekhalo.info.domain.info.video.update;

import com.geekhalo.lego.core.command.CommandForUpdateById;
import java.lang.Long;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class UpdateVideoInfoCommand implements CommandForUpdateById<Long> {
    @NotNull
    private Long id;
    @NotNull
    private String title;
    @NotNull
    private Long duration;
    @NotNull
    private String videoUrl;

    public UpdateVideoInfoCommand(Long id){
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public static UpdateVideoInfoCommand apply(Long id){
        UpdateVideoInfoCommand command = new UpdateVideoInfoCommand(id); 
        return command;
    }
}
