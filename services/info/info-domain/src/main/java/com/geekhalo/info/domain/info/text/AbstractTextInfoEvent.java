package com.geekhalo.info.domain.info.text;

import com.geekhalo.lego.core.command.support.AbstractDomainEvent;
import java.lang.Long;

public abstract class AbstractTextInfoEvent
        extends AbstractDomainEvent<Long, TextInfo> {
    public AbstractTextInfoEvent(TextInfo agg) {
        super(agg);
    }
}
