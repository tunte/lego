package com.geekhalo.info.domain.info.picture;


import com.geekhalo.lego.core.command.CommandRepository;
import com.geekhalo.info.domain.info.picture.PictureInfo;
import java.lang.Long;

import java.util.Optional;

public interface PictureInfoCommandRepository extends CommandRepository<PictureInfo, Long> {



}
