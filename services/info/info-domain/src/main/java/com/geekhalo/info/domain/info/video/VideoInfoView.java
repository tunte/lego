package com.geekhalo.info.domain.info.video;

import com.geekhalo.info.domain.info.BaseInfoView;
import com.geekhalo.lego.core.query.AbstractView;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Getter;

import javax.persistence.*;

@Entity
@Table(name = "video_info")
@PrimaryKeyJoinColumn(name = "info_id")
@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Setter(AccessLevel.PRIVATE)
public class VideoInfoView extends BaseInfoView {

    private Long duration;

    private String videoUrl;

}