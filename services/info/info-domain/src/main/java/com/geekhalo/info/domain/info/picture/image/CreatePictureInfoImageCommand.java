package com.geekhalo.info.domain.info.picture.image;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CreatePictureInfoImageCommand {
    @NotNull
    private String title;

    @NotNull
    private String imageUrl;

    @NotNull
    private String detail;

    @NotNull
    private Integer showOrder;
}
