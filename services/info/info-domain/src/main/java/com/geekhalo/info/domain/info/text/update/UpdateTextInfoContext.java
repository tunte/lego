package com.geekhalo.info.domain.info.text.update;

import com.geekhalo.info.domain.info.text.update.UpdateTextInfoCommand; 
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class UpdateTextInfoContext{
    private UpdateTextInfoCommand command;

    private UpdateTextInfoContext(UpdateTextInfoCommand command){
         this.command = command;
    }

    public static UpdateTextInfoContext apply(UpdateTextInfoCommand command) {
        UpdateTextInfoContext context = new UpdateTextInfoContext(command);
        return context;
    }
}
