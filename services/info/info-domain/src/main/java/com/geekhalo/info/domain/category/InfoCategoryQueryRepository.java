package com.geekhalo.info.domain.category;


import com.geekhalo.lego.core.query.QueryRepository;
import com.geekhalo.info.domain.category.InfoCategoryView;
import java.lang.Long;

import java.util.Optional;

public interface InfoCategoryQueryRepository extends QueryRepository<InfoCategoryView, Long> {



}
