package com.geekhalo.info.domain.info.video.offline;

import com.geekhalo.lego.core.command.CommandForUpdateById;
import java.lang.Long;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class OfflineVideoInfoCommand implements CommandForUpdateById<Long> {
    @NotNull
    private Long id;

    public OfflineVideoInfoCommand(Long id){
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public static OfflineVideoInfoCommand apply(Long id){
        OfflineVideoInfoCommand command = new OfflineVideoInfoCommand(id); 
        return command;
    }
}
