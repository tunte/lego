package com.geekhalo.info.domain.info.picture;

import com.geekhalo.info.domain.info.BaseInfoView;
import com.geekhalo.info.domain.info.picture.image.JoinPictureInfoImageByInfoId;
import com.geekhalo.info.domain.info.picture.image.PictureInfoImage;
import com.geekhalo.info.domain.info.picture.image.PictureInfoImageView;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "picture_info")
@PrimaryKeyJoinColumn(name = "info_id")
@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Setter(AccessLevel.PRIVATE)
public class PictureInfoView extends BaseInfoView {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer imageCount;

    @Transient
    @JoinPictureInfoImageByInfoId(keyFromSourceData = "#{id}")
    @Setter(AccessLevel.PUBLIC)
    private List<PictureInfoImageView> images;

}