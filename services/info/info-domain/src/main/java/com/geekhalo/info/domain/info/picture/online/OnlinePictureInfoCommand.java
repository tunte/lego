package com.geekhalo.info.domain.info.picture.online;

import com.geekhalo.lego.core.command.CommandForUpdateById;
import java.lang.Long;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class OnlinePictureInfoCommand implements CommandForUpdateById<Long> {
    @NotNull
    private Long id;

    public OnlinePictureInfoCommand(Long id){
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public static OnlinePictureInfoCommand apply(Long id){
        OnlinePictureInfoCommand command = new OnlinePictureInfoCommand(id); 
        return command;
    }
}
