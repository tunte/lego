package com.geekhalo.info.domain.info.picture.image;

import com.geekhalo.lego.core.command.support.AbstractEntity;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;

@Entity
@Table(name = "picture_info_image")
@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Setter(AccessLevel.PRIVATE)
public class PictureInfoImage extends AbstractEntity { 
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    private String imageUrl;

    private String detail;

    private Integer showOrder;

    public static PictureInfoImage create(CreatePictureInfoImageCommand command){
        PictureInfoImage infoImage = new PictureInfoImage();
        infoImage.setImageUrl(command.getImageUrl());
        infoImage.setTitle(command.getTitle());
        infoImage.setDetail(command.getDetail());
        infoImage.setShowOrder(command.getShowOrder());
        return infoImage;
    }

    public void updateBy(UpdatePictureInfoImageCommand updateCommand) {
        if (StringUtils.isNotEmpty(updateCommand.getTitle())){
            setTitle(updateCommand.getTitle());
        }
        if (StringUtils.isNotEmpty(updateCommand.getImageUrl())){
            setImageUrl(updateCommand.getImageUrl());
        }
        if (StringUtils.isNotEmpty(updateCommand.getDetail())){
            setDetail(updateCommand.getDetail());
        }
        if (updateCommand.getShowOrder() != null){
            setShowOrder(updateCommand.getShowOrder());
        }
    }
}