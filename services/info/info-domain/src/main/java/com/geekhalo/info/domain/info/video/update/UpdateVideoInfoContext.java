package com.geekhalo.info.domain.info.video.update;

import com.geekhalo.info.domain.info.video.update.UpdateVideoInfoCommand; 
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class UpdateVideoInfoContext{
    private UpdateVideoInfoCommand command;

    private UpdateVideoInfoContext(UpdateVideoInfoCommand command){
         this.command = command;
    }

    public static UpdateVideoInfoContext apply(UpdateVideoInfoCommand command) {
        UpdateVideoInfoContext context = new UpdateVideoInfoContext(command);
        return context;
    }
}
