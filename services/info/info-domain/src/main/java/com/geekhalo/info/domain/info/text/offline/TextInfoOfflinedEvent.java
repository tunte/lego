package com.geekhalo.info.domain.info.text.offline;

import lombok.Value;
import com.geekhalo.info.domain.info.text.AbstractTextInfoEvent;
import com.geekhalo.info.domain.info.text.TextInfo;

@Value
public class TextInfoOfflinedEvent
        extends AbstractTextInfoEvent{
    public TextInfoOfflinedEvent(TextInfo agg){
        super(agg);
    }
}
