package com.geekhalo.info.domain.info.picture.offline;

import lombok.Value;
import com.geekhalo.info.domain.info.picture.AbstractPictureInfoEvent;
import com.geekhalo.info.domain.info.picture.PictureInfo;

@Value
public class PictureInfoOfflinedEvent
        extends AbstractPictureInfoEvent{
    public PictureInfoOfflinedEvent(PictureInfo agg){
        super(agg);
    }
}
