package com.geekhalo.info.domain.info.text.offline;

import com.geekhalo.info.domain.info.text.offline.OfflineTextInfoCommand; 
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class OfflineTextInfoContext{
    private OfflineTextInfoCommand command;

    private OfflineTextInfoContext(OfflineTextInfoCommand command){
         this.command = command;
    }

    public static OfflineTextInfoContext apply(OfflineTextInfoCommand command) {
        OfflineTextInfoContext context = new OfflineTextInfoContext(command);
        return context;
    }
}
