package com.geekhalo.info.domain.info;

import com.geekhalo.lego.core.command.support.AbstractAggRoot;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Setter(AccessLevel.PROTECTED)
@Table(name = "base_info")
@Entity
// 当前策略为Joined策略
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class BaseInfo extends AbstractAggRoot {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.PUBLIC)
    private Long id;

    private Long categoryId;

    private String title;

    private InfoStatus status;

    @Override
    public Long getId() {
        return id;
    }

    protected boolean setOnline(){
        if (this.getStatus() == InfoStatus.ONLINE){
            return false;
        }
        setStatus(InfoStatus.ONLINE);
        return true;
    }

    protected boolean setOffline(){
        if (getStatus() == InfoStatus.OFFLINE){
            return false;
        }
        setStatus(InfoStatus.OFFLINE);
        return true;
    }
}
