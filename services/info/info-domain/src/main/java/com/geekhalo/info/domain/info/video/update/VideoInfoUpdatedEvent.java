package com.geekhalo.info.domain.info.video.update;

import lombok.Value;
import com.geekhalo.info.domain.info.video.AbstractVideoInfoEvent;
import com.geekhalo.info.domain.info.video.VideoInfo;

@Value
public class VideoInfoUpdatedEvent
        extends AbstractVideoInfoEvent{
    public VideoInfoUpdatedEvent(VideoInfo agg){
        super(agg);
    }
}
