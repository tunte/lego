package com.geekhalo.info.domain.info.picture.create;

import lombok.Value;
import com.geekhalo.info.domain.info.picture.AbstractPictureInfoEvent;
import com.geekhalo.info.domain.info.picture.PictureInfo;

@Value
public class PictureInfoCreatedEvent
        extends AbstractPictureInfoEvent{
    public PictureInfoCreatedEvent(PictureInfo agg){
        super(agg);
    }
}
