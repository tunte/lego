package com.geekhalo.info.domain.info.text.online;

import com.geekhalo.info.domain.info.text.online.OnlineTextInfoCommand; 
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class OnlineTextInfoContext{
    private OnlineTextInfoCommand command;

    private OnlineTextInfoContext(OnlineTextInfoCommand command){
         this.command = command;
    }

    public static OnlineTextInfoContext apply(OnlineTextInfoCommand command) {
        OnlineTextInfoContext context = new OnlineTextInfoContext(command);
        return context;
    }
}
