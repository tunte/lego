package com.geekhalo.info.domain.info.text.offline;

import com.geekhalo.lego.core.command.CommandForUpdateById;
import java.lang.Long;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class OfflineTextInfoCommand implements CommandForUpdateById<Long> {
    @NotNull
    private Long id;

    public OfflineTextInfoCommand(Long id){
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public static OfflineTextInfoCommand apply(Long id){
        OfflineTextInfoCommand command = new OfflineTextInfoCommand(id); 
        return command;
    }
}
