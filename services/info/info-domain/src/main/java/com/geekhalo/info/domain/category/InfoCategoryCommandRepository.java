package com.geekhalo.info.domain.category;


import com.geekhalo.lego.core.command.CommandRepository;
import com.geekhalo.info.domain.category.InfoCategory;
import java.lang.Long;

import java.util.Optional;

public interface InfoCategoryCommandRepository extends CommandRepository<InfoCategory, Long> {



}
