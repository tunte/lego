package com.geekhalo.info.domain.category.enable;

import com.geekhalo.info.domain.category.enable.EnableInfoCategoryCommand; 
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class EnableInfoCategoryContext{
    private EnableInfoCategoryCommand command;

    private EnableInfoCategoryContext(EnableInfoCategoryCommand command){
         this.command = command;
    }

    public static EnableInfoCategoryContext apply(EnableInfoCategoryCommand command) {
        EnableInfoCategoryContext context = new EnableInfoCategoryContext(command);
        return context;
    }
}
