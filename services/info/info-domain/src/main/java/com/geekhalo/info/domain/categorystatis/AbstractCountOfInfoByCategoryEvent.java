package com.geekhalo.info.domain.categorystatis;

import com.geekhalo.lego.core.command.support.AbstractDomainEvent;
import java.lang.Long;

public abstract class AbstractCountOfInfoByCategoryEvent
        extends AbstractDomainEvent<Long, CountOfInfoByCategory> {
    public AbstractCountOfInfoByCategoryEvent(CountOfInfoByCategory agg) {
        super(agg);
    }
}
