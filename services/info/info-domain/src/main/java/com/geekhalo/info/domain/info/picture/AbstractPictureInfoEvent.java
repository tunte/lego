package com.geekhalo.info.domain.info.picture;

import com.geekhalo.lego.core.command.support.AbstractDomainEvent;
import java.lang.Long;

public abstract class AbstractPictureInfoEvent
        extends AbstractDomainEvent<Long, PictureInfo> {
    public AbstractPictureInfoEvent(PictureInfo agg) {
        super(agg);
    }
}
