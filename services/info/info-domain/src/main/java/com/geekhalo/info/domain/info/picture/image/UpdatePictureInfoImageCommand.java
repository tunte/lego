package com.geekhalo.info.domain.info.picture.image;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UpdatePictureInfoImageCommand {
    @NotNull
    private Long imageId;

    @NotNull
    private String title;

    @NotNull
    private String imageUrl;

    @NotNull
    private String detail;

    @NotNull
    private Integer showOrder;
}
