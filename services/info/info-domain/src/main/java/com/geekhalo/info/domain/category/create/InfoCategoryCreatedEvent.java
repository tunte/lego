package com.geekhalo.info.domain.category.create;

import lombok.Value;
import com.geekhalo.info.domain.category.AbstractInfoCategoryEvent;
import com.geekhalo.info.domain.category.InfoCategory;

@Value
public class InfoCategoryCreatedEvent
        extends AbstractInfoCategoryEvent{
    public InfoCategoryCreatedEvent(InfoCategory agg){
        super(agg);
    }
}
