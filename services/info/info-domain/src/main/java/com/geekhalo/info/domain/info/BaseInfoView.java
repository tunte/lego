package com.geekhalo.info.domain.info;

import com.geekhalo.info.domain.category.InfoCategoryView;
import com.geekhalo.info.domain.category.JoinInfoCategoryById;
import com.geekhalo.lego.core.query.AbstractView;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Setter(AccessLevel.PRIVATE)
@Table(name = "base_info")
@Entity
// 当前策略为Joined策略
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class BaseInfoView extends AbstractView {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long categoryId;

    @Transient
    @JoinInfoCategoryById(keyFromSourceData = "#{categoryId}")
    @Setter(AccessLevel.PUBLIC)
    private InfoCategoryView category;

    private String title;

    private InfoStatus status;
}
