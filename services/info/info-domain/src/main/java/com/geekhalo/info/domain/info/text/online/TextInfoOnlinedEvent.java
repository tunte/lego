package com.geekhalo.info.domain.info.text.online;

import lombok.Value;
import com.geekhalo.info.domain.info.text.AbstractTextInfoEvent;
import com.geekhalo.info.domain.info.text.TextInfo;

@Value
public class TextInfoOnlinedEvent
        extends AbstractTextInfoEvent{
    public TextInfoOnlinedEvent(TextInfo agg){
        super(agg);
    }
}
