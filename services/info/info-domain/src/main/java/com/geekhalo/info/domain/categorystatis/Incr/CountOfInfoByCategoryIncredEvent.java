package com.geekhalo.info.domain.categorystatis.Incr;

import lombok.Value;
import com.geekhalo.info.domain.categorystatis.AbstractCountOfInfoByCategoryEvent;
import com.geekhalo.info.domain.categorystatis.CountOfInfoByCategory;

@Value
public class CountOfInfoByCategoryIncredEvent
        extends AbstractCountOfInfoByCategoryEvent{
    public CountOfInfoByCategoryIncredEvent(CountOfInfoByCategory agg){
        super(agg);
    }
}
