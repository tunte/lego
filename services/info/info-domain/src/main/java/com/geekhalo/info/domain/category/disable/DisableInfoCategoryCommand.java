package com.geekhalo.info.domain.category.disable;

import com.geekhalo.lego.core.command.CommandForUpdateById;
import java.lang.Long;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class DisableInfoCategoryCommand implements CommandForUpdateById<Long> {
    @NotNull
    private Long id;

    public DisableInfoCategoryCommand(Long id){
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public static DisableInfoCategoryCommand apply(Long id){
        DisableInfoCategoryCommand command = new DisableInfoCategoryCommand(id); 
        return command;
    }
}
