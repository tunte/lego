package com.geekhalo.info.domain.info.text.update;

import com.geekhalo.lego.core.command.CommandForUpdateById;
import java.lang.Long;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class UpdateTextInfoCommand implements CommandForUpdateById<Long> {
    @NotNull
    private Long id;
    private String title;
    private String detail;

    public UpdateTextInfoCommand(Long id){
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public static UpdateTextInfoCommand apply(Long id){
        UpdateTextInfoCommand command = new UpdateTextInfoCommand(id); 
        return command;
    }
}
