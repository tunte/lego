package com.geekhalo.info.domain.info.picture.image;

import com.geekhalo.lego.core.query.AbstractView;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Getter;

import javax.persistence.*;

@Entity
@Table(name = "picture_info_image")
@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Setter(AccessLevel.PRIVATE)
public class PictureInfoImageView extends AbstractView { 
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    private String imageUrl;

    private String detail;

    @Column(name = "info_id")
    private Long infoId;
}