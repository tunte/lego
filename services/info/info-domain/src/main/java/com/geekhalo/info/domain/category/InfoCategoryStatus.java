package com.geekhalo.info.domain.category;

import com.geekhalo.lego.common.enums.CommonEnum;

public enum InfoCategoryStatus implements CommonEnum {
    ENABLE(1, "可用"),
    DISABLE(2, "禁用");
    private final int code;
    private final String descr;

    InfoCategoryStatus(int code, String descr){
        this.code = code;
        this.descr = descr;
    }
    @Override
    public int getCode() {
        return this.code;
    }

    @Override
    public String getDescription() {
        return this.descr;
    }
}
