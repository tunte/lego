package com.geekhalo.info.domain.info.picture.online;

import lombok.Value;
import com.geekhalo.info.domain.info.picture.AbstractPictureInfoEvent;
import com.geekhalo.info.domain.info.picture.PictureInfo;

@Value
public class PictureInfoOnlinedEvent
        extends AbstractPictureInfoEvent{
    public PictureInfoOnlinedEvent(PictureInfo agg){
        super(agg);
    }
}
