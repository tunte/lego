package com.geekhalo.info.domain.info.video;

import com.geekhalo.lego.core.command.support.AbstractDomainEvent;
import java.lang.Long;

public abstract class AbstractVideoInfoEvent
        extends AbstractDomainEvent<Long, VideoInfo> {
    public AbstractVideoInfoEvent(VideoInfo agg) {
        super(agg);
    }
}
