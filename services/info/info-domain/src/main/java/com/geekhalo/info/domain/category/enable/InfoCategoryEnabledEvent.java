package com.geekhalo.info.domain.category.enable;

import lombok.Value;
import com.geekhalo.info.domain.category.AbstractInfoCategoryEvent;
import com.geekhalo.info.domain.category.InfoCategory;

@Value
public class InfoCategoryEnabledEvent
        extends AbstractInfoCategoryEvent{
    public InfoCategoryEnabledEvent(InfoCategory agg){
        super(agg);
    }
}
