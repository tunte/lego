package com.geekhalo.info.config;

import com.geekhalo.lego.core.command.EnableCommandService;
import com.geekhalo.lego.core.query.EnableQueryService;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableCommandService(basePackages = "com.geekhalo.info.app")
@EnableQueryService(basePackages = "com.geekhalo.info.app")
public class ApplicationServiceConfiguration {
}
