package com.geekhalo.info.app.info.picture;

import com.geekhalo.info.Application;
import com.geekhalo.info.app.info.text.PageByCategoryId;
import com.geekhalo.info.domain.info.InfoStatus;
import com.geekhalo.info.domain.info.picture.PictureInfo;
import com.geekhalo.info.domain.info.picture.PictureInfoView;
import com.geekhalo.info.domain.info.picture.create.CreatePictureInfoCommand;
import com.geekhalo.info.domain.info.picture.image.CreatePictureInfoImageCommand;
import com.geekhalo.info.domain.info.picture.image.PictureInfoImageView;
import com.geekhalo.info.domain.info.picture.image.RemovePictureInfoImageCommand;
import com.geekhalo.info.domain.info.picture.image.UpdatePictureInfoImageCommand;
import com.geekhalo.info.domain.info.picture.offline.OfflinePictureInfoCommand;
import com.geekhalo.info.domain.info.picture.online.OnlinePictureInfoCommand;
import com.geekhalo.info.domain.info.picture.update.UpdatePictureInfoCommand;
import com.geekhalo.info.domain.info.text.TextInfoView;
import com.geekhalo.lego.core.singlequery.Page;
import com.geekhalo.lego.core.singlequery.Pageable;
import org.apache.commons.collections.CollectionUtils;
import org.assertj.core.util.Lists;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest(classes = Application.class)
public class PictureInfoApplicationTest {
    @Autowired
    private PictureInfoCommandApplication commandApplication;
    @Autowired
    private PictureInfoQueryApplication queryApplication;

    private PictureInfo pictureInfo;

    @BeforeEach
    public void init(){
        this.pictureInfo = createPictureInfo();
        Assert.assertNotNull(this.pictureInfo);
        Assert.assertNotNull(this.pictureInfo.getId());
    }

    @Test
    public void createTest(){
        PictureInfo pictureInfo = this.commandApplication.findById(this.pictureInfo.getId()).get();
        Assert.assertEquals(this.pictureInfo.getId(), pictureInfo.getId());
        Assert.assertEquals(this.pictureInfo.getTitle(), pictureInfo.getTitle());
        Assert.assertEquals(this.pictureInfo.getImageCount(), pictureInfo.getImageCount());
        Assert.assertEquals(this.pictureInfo.getStatus(), pictureInfo.getStatus());
        Assert.assertEquals(this.pictureInfo.getImages().size(), pictureInfo.getImages().size());
    }

    @Test
    public void onlineOfflineTest(){
        {
            PictureInfoView pictureInfoView = this.queryApplication.getById(this.pictureInfo.getId());
            Assert.assertEquals(this.pictureInfo.getId(), pictureInfoView.getId());
            Assert.assertEquals(InfoStatus.ONLINE, pictureInfoView.getStatus());
        }

        this.commandApplication.offline(new OfflinePictureInfoCommand(this.pictureInfo.getId()));

        {
            PictureInfoView pictureInfoView = this.queryApplication.getById(this.pictureInfo.getId());
            Assert.assertEquals(this.pictureInfo.getId(), pictureInfoView.getId());
            Assert.assertEquals(InfoStatus.OFFLINE, pictureInfoView.getStatus());
        }

        this.commandApplication.online(new OnlinePictureInfoCommand(this.pictureInfo.getId()));

        {
            PictureInfoView pictureInfoView = this.queryApplication.getById(this.pictureInfo.getId());
            Assert.assertEquals(this.pictureInfo.getId(), pictureInfoView.getId());
            Assert.assertEquals(InfoStatus.ONLINE, pictureInfoView.getStatus());
        }
    }

    @Test
    public void updateTest(){
        PictureInfoView view = this.queryApplication.getById(this.pictureInfo.getId());
        String title = "新的测试标题";
        UpdatePictureInfoCommand command = new UpdatePictureInfoCommand(this.pictureInfo.getId());
        command.setTitle(title);
        List<RemovePictureInfoImageCommand> removeCommands = Lists.newArrayList();
        command.setRemoveImageCommands(removeCommands);
        for (int i=0;i<3;i++ ){
            PictureInfoImageView infoImageView = view.getImages().get(i);
            RemovePictureInfoImageCommand removeCommand = new RemovePictureInfoImageCommand();
            removeCommands.add(removeCommand);
            removeCommand.setImageId(infoImageView.getId());
        }

        List<UpdatePictureInfoImageCommand> updateCommands = Lists.newArrayList();
        command.setUpdateImageCommands(updateCommands);
        for (int i=3;i<5;i++){
            PictureInfoImageView infoImageView = view.getImages().get(i);
            UpdatePictureInfoImageCommand updateCommand = new UpdatePictureInfoImageCommand();
            updateCommand.setImageId(infoImageView.getId());
            updateCommand.setTitle("新标题-" + (i+1));
            updateCommand.setDetail("新详情-" + (i+1));
            updateCommand.setShowOrder((i+1));
            updateCommand.setImageUrl("新地址-" + (i+1));
            updateCommands.add(updateCommand);
        }

        List<CreatePictureInfoImageCommand> createCommands = Lists.newArrayList();
        command.setCreateImageCommands(createCommands);
        for (int i=0; i<6; i++){
            CreatePictureInfoImageCommand createCommand = new CreatePictureInfoImageCommand();
            createCommand.setTitle("新增标题-" + (i+1));
            createCommand.setDetail("新增详情-" + (i+1));
            createCommand.setShowOrder((i+1));
            createCommand.setImageUrl("新增地址-" + (i+1));
            createCommands.add(createCommand);
        }

        this.commandApplication.update(command);


        PictureInfoView viewToCheck = this.queryApplication.getById(this.pictureInfo.getId());
        Assert.assertEquals(title, viewToCheck.getTitle());
        Assert.assertEquals(8, viewToCheck.getImageCount().intValue());
        Assert.assertNotNull(viewToCheck.getImages());

    }

    @Test
    public void pageOfTest(){
        PageByCategory pageByCategory = new PageByCategory();
        pageByCategory.setCategoryId(2L);
        Pageable pageable = Pageable.builder()
                .pageNo(0)
                .pageSize(10)
                .build();
        pageByCategory.setPageable(pageable);

        Page<PictureInfoView> infoViewPage = this.queryApplication.pageOf(pageByCategory);
        Assert.assertNotNull(infoViewPage);
        Assert.assertTrue(infoViewPage.hasContent());
        Assert.assertNotNull(infoViewPage.getContent());
        Assert.assertTrue(CollectionUtils.isNotEmpty(infoViewPage.getContent()));
    }

    private PictureInfo createPictureInfo() {
        CreatePictureInfoCommand command = new CreatePictureInfoCommand();
        command.setCategoryId(2L);
        command.setTitle("图文资讯");
        List<CreatePictureInfoImageCommand> imageCommands = new ArrayList<>();
        command.setImages(imageCommands);
        for (int i =0; i< 5; i++){
            CreatePictureInfoImageCommand imageCommand = new CreatePictureInfoImageCommand();
            imageCommands.add(imageCommand);
            imageCommand.setTitle("图片-" + (i + 1));
            imageCommand.setDetail("图片描述-" + (i + 1));
            imageCommand.setImageUrl("地址" + (i+1));
            imageCommand.setShowOrder(i + 1);
        }
        return this.commandApplication.create(command);
    }
}
