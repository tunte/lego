package com.geekhalo.info.app.info.video;

import com.geekhalo.info.Application;
import com.geekhalo.info.app.info.video.query.PageByCategory;
import com.geekhalo.info.domain.info.InfoStatus;
import com.geekhalo.info.domain.info.text.TextInfoView;
import com.geekhalo.info.domain.info.video.VideoInfo;
import com.geekhalo.info.domain.info.video.VideoInfoView;
import com.geekhalo.info.domain.info.video.create.CreateVideoInfoCommand;
import com.geekhalo.info.domain.info.video.offline.OfflineVideoInfoCommand;
import com.geekhalo.info.domain.info.video.online.OnlineVideoInfoCommand;
import com.geekhalo.lego.core.singlequery.Page;
import com.geekhalo.lego.core.singlequery.Pageable;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = Application.class)
public class VideoInfoApplicationTest {
    @Autowired
    private VideoInfoCommandApplication commandApplication;
    @Autowired
    private VideoInfoQueryApplication queryApplication;

    private VideoInfo videoInfo;

    @BeforeEach
    public void init(){
        this.videoInfo = create();
        Assert.assertNotNull(this.videoInfo);
        Assert.assertNotNull(this.videoInfo.getId());
    }

    @Test
    public void getById(){
        VideoInfo videoInfo = this.commandApplication.findById(this.videoInfo.getId()).get();
        Assert.assertNotNull(videoInfo);

        Assert.assertEquals(this.videoInfo.getId(), videoInfo.getId());
        Assert.assertEquals(this.videoInfo.getVideoUrl(), videoInfo.getVideoUrl());
        Assert.assertEquals(this.videoInfo.getTitle(), videoInfo.getTitle());
        Assert.assertEquals(this.videoInfo.getStatus(), videoInfo.getStatus());
        Assert.assertEquals(this.videoInfo.getDuration(), videoInfo.getDuration());
    }

    @Test
    public void offlineOrOnline(){
        {
            VideoInfo videoInfo = this.commandApplication.findById(this.videoInfo.getId()).get();
            Assert.assertEquals(InfoStatus.ONLINE, videoInfo.getStatus());
        }

        this.commandApplication.offline(new OfflineVideoInfoCommand(this.videoInfo.getId()));
        {
            VideoInfo videoInfo = this.commandApplication.findById(this.videoInfo.getId()).get();
            Assert.assertEquals(InfoStatus.OFFLINE, videoInfo.getStatus());
        }

        this.commandApplication.online(new OnlineVideoInfoCommand(this.videoInfo.getId()));
        {
            VideoInfo videoInfo = this.commandApplication.findById(this.videoInfo.getId()).get();
            Assert.assertEquals(InfoStatus.ONLINE, videoInfo.getStatus());
        }
    }

    @Test
    public void pageByCategory(){
        PageByCategory pageByCategoryId = new PageByCategory();
        pageByCategoryId.setCategoryId(2L);
        Pageable pageable = Pageable.builder()
                .pageNo(0)
                .pageSize(10)
                .build();
        pageByCategoryId.setPageable(pageable);

        Page<VideoInfoView> videoInfoPage = this.queryApplication.pageOf(pageByCategoryId);
        Assert.assertNotNull(videoInfoPage);
        Assert.assertTrue(videoInfoPage.hasContent());
        Assert.assertNotNull(videoInfoPage.getContent());
        Assert.assertTrue(CollectionUtils.isNotEmpty(videoInfoPage.getContent()));
    }

    private VideoInfo create() {
        CreateVideoInfoCommand command = new CreateVideoInfoCommand();
        command.setCategoryId(2L);
        command.setTitle("测试");
        command.setVideoUrl("测试地址");
        command.setDuration(1000L);

        return this.commandApplication.create(command);
    }
}
