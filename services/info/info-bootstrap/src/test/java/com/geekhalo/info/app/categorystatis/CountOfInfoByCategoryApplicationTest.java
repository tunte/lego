package com.geekhalo.info.app.categorystatis;

import com.geekhalo.info.Application;
import com.geekhalo.info.domain.categorystatis.CountOfInfoByCategory;
import com.geekhalo.info.domain.categorystatis.CountOfInfoByCategoryView;
import com.geekhalo.info.domain.categorystatis.Incr.IncrCountOfInfoByCategoryCommand;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = Application.class)
public class CountOfInfoByCategoryApplicationTest {
    @Autowired
    private CountOfInfoByCategoryCommandApplication commandApplication;
    @Autowired
    private CountOfInfoByCategoryQueryApplication queryApplication;

    private Long categoryId;

    @BeforeEach
    public void init(){
        this.categoryId = RandomUtils.nextLong(0, Long.MAX_VALUE);
        IncrCountOfInfoByCategoryCommand command = new IncrCountOfInfoByCategoryCommand();
        command.setKey(this.categoryId);
        command.setCount(1);
        this.commandApplication.incr(command);
    }

    @Test
    public void incr(){
        Integer count = 0;
        {
            CountOfInfoByCategoryView view = this.queryApplication.getByCategoryId(this.categoryId);
            Assert.assertNotNull(view);
            Assert.assertEquals(this.categoryId, view.getCategoryId());
            count = view.getCount();
        }

        IncrCountOfInfoByCategoryCommand command = new IncrCountOfInfoByCategoryCommand();
        command.setKey(this.categoryId);
        command.setCount(1);

        this.commandApplication.incr(command);
        {
            CountOfInfoByCategoryView view = this.queryApplication.getByCategoryId(this.categoryId);
            Assert.assertNotNull(view);
            Assert.assertEquals(this.categoryId, view.getCategoryId());
            Assert.assertEquals(count + 1, view.getCount().intValue());
        }
    }
}
