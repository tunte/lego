package com.geekhalo.info.app.info.text;

import com.geekhalo.info.Application;
import com.geekhalo.info.domain.info.InfoStatus;
import com.geekhalo.info.domain.info.text.TextInfo;
import com.geekhalo.info.domain.info.text.TextInfoView;
import com.geekhalo.info.domain.info.text.create.CreateTextInfoCommand;
import com.geekhalo.info.domain.info.text.offline.OfflineTextInfoCommand;
import com.geekhalo.info.domain.info.text.online.OnlineTextInfoCommand;
import com.geekhalo.info.domain.info.text.update.UpdateTextInfoCommand;
import com.geekhalo.lego.core.singlequery.Page;
import com.geekhalo.lego.core.singlequery.Pageable;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = Application.class)
public class TextInfoApplication {
    @Autowired
    private TextInfoCommandApplication commandApplication;

    @Autowired
    private TextInfoQueryApplication queryApplication;

    private TextInfo textInfo;

    @BeforeEach
    public void init(){
        this.textInfo = createTextInfo();
        Assert.assertNotNull(this.textInfo);
    }

    @Test
    public void create(){
        TextInfoView textInfoView = this.queryApplication.getById(this.textInfo.getId());

        Assert.assertEquals(this.textInfo.getId(), textInfoView.getId());
        Assert.assertEquals(this.textInfo.getTitle(), textInfoView.getTitle());
        Assert.assertEquals(this.textInfo.getDetail(), textInfoView.getDetail());
        Assert.assertEquals(this.textInfo.getStatus(), textInfoView.getStatus());
        Assert.assertNotNull(textInfoView.getCategory());
    }

    @Test
    public void update(){
        String title = "测试标题111";
        String detail = "测试详情2222";

        UpdateTextInfoCommand command = new UpdateTextInfoCommand();
        command.setId(this.textInfo.getId());
        command.setTitle(title);
        command.setDetail(detail);
        this.commandApplication.update(command);

        TextInfoView textInfoView = this.queryApplication.getById(this.textInfo.getId());
        Assert.assertEquals(this.textInfo.getId(), textInfoView.getId());
        Assert.assertEquals(title, textInfoView.getTitle());
        Assert.assertEquals(detail, textInfoView.getDetail());
        Assert.assertEquals(this.textInfo.getStatus(), textInfoView.getStatus());

    }

    @Test
    public void onlineOrOffline(){
        {
            TextInfoView textInfoView = this.queryApplication.getById(this.textInfo.getId());
            Assert.assertEquals(this.textInfo.getId(), textInfoView.getId());
            Assert.assertEquals(InfoStatus.ONLINE, textInfoView.getStatus());
        }

        this.commandApplication.offline(new OfflineTextInfoCommand(this.textInfo.getId()));
        {
            TextInfoView textInfoView = this.queryApplication.getById(this.textInfo.getId());
            Assert.assertEquals(this.textInfo.getId(), textInfoView.getId());
            Assert.assertEquals(InfoStatus.OFFLINE, textInfoView.getStatus());
        }

        this.commandApplication.online(new OnlineTextInfoCommand(this.textInfo.getId()));
        {
            TextInfoView textInfoView = this.queryApplication.getById(this.textInfo.getId());
            Assert.assertEquals(this.textInfo.getId(), textInfoView.getId());
            Assert.assertEquals(InfoStatus.ONLINE, textInfoView.getStatus());
        }
    }

    @Test
    public void pageByCategoryId(){
        PageByCategoryId pageByCategoryId = new PageByCategoryId();
        pageByCategoryId.setCategoryId(2L);
        Pageable pageable = Pageable.builder()
                .pageNo(0)
                .pageSize(10)
                .build();
        pageByCategoryId.setPageable(pageable);

        Page<TextInfoView> textInfoViewPage = this.queryApplication.pageOf(pageByCategoryId);
        Assert.assertNotNull(textInfoViewPage);
        Assert.assertTrue(textInfoViewPage.hasContent());
        Assert.assertNotNull(textInfoViewPage.getContent());
        Assert.assertTrue(CollectionUtils.isNotEmpty(textInfoViewPage.getContent()));
    }

    private TextInfo createTextInfo() {
        CreateTextInfoCommand command = new CreateTextInfoCommand();
        command.setCategoryId(2L);
        command.setTitle("测试标题");
        command.setDetail("测试详情");
        return this.commandApplication.create(command);
    }
}
