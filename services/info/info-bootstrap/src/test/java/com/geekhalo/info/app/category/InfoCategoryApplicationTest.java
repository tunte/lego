package com.geekhalo.info.app.category;

import com.geekhalo.info.Application;
import com.geekhalo.info.domain.category.InfoCategory;
import com.geekhalo.info.domain.category.InfoCategoryQueryRepository;
import com.geekhalo.info.domain.category.InfoCategoryStatus;
import com.geekhalo.info.domain.category.InfoCategoryView;
import com.geekhalo.info.domain.category.create.CreateInfoCategoryCommand;
import com.geekhalo.info.domain.category.disable.DisableInfoCategoryCommand;
import com.geekhalo.info.domain.category.enable.EnableInfoCategoryCommand;
import com.geekhalo.info.domain.category.update.UpdateInfoCategoryCommand;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;

@SpringBootTest(classes = Application.class)
//@Transactional
public class InfoCategoryApplicationTest {
    @Autowired
    private InfoCategoryCommandApplication commandApplication;
    @Autowired
    private InfoCategoryQueryApplication queryApplication;

    private InfoCategory infoCategory;

    @BeforeEach
    public void init(){
        this.infoCategory = createCategory();
        Assert.assertNotNull(this.infoCategory);
    }

    @Test
    public void create(){
        Assert.assertNotNull(infoCategory.getId());
    }

    @Test
    public void queryById(){
        InfoCategoryView categoryView = this.queryApplication.getById(this.infoCategory.getId());
        Assert.assertEquals(this.infoCategory.getId(), categoryView.getId());
        Assert.assertEquals(this.infoCategory.getName(), categoryView.getName());
        Assert.assertEquals(this.infoCategory.getShowOrder(), categoryView.getShowOrder());
        Assert.assertEquals(this.infoCategory.getStatus(), categoryView.getStatus());
    }

    @Test
    public void updateById(){
        String name = "测1111";
        Integer showOrder = 1000;
        UpdateInfoCategoryCommand command = new UpdateInfoCategoryCommand(this.infoCategory.getId());
        command.setName(name);
        command.setShowOrder(showOrder);
        this.commandApplication.update(command);

        InfoCategoryView categoryView = this.queryApplication.getById(this.infoCategory.getId());
        Assert.assertEquals(this.infoCategory.getId(), categoryView.getId());
        Assert.assertEquals(name, categoryView.getName());
        Assert.assertEquals(showOrder, categoryView.getShowOrder());
        Assert.assertEquals(this.infoCategory.getStatus(), categoryView.getStatus());
    }

    @Test
    public void statusTest(){
        {
            InfoCategoryView categoryView = this.queryApplication.getById(this.infoCategory.getId());
            Assert.assertEquals(this.infoCategory.getId(), categoryView.getId());
            Assert.assertEquals(InfoCategoryStatus.ENABLE, categoryView.getStatus());
        }

        this.commandApplication.disable(new DisableInfoCategoryCommand(this.infoCategory.getId()));

        {
            InfoCategoryView categoryView = this.queryApplication.getById(this.infoCategory.getId());
            Assert.assertEquals(this.infoCategory.getId(), categoryView.getId());
            Assert.assertEquals(InfoCategoryStatus.DISABLE, categoryView.getStatus());
        }

        this.commandApplication.enable(new EnableInfoCategoryCommand(this.infoCategory.getId()));

        {
            InfoCategoryView categoryView = this.queryApplication.getById(this.infoCategory.getId());
            Assert.assertEquals(this.infoCategory.getId(), categoryView.getId());
            Assert.assertEquals(InfoCategoryStatus.ENABLE, categoryView.getStatus());
        }
    }

    private InfoCategory createCategory() {
        CreateInfoCategoryCommand command = new CreateInfoCategoryCommand();
        command.setName("测试分类");
        command.setShowOrder(1);
        InfoCategory infoCategory = this.commandApplication.create(command);
        return infoCategory;
    }
}
