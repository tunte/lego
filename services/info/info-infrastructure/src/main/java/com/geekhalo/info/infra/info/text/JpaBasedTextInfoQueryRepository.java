package com.geekhalo.info.infra.info.text;


import org.springframework.data.jpa.repository.JpaRepository;
import com.geekhalo.info.domain.info.text.TextInfoQueryRepository;
import com.geekhalo.info.domain.info.text.TextInfoView;
import java.lang.Long;

public interface JpaBasedTextInfoQueryRepository extends TextInfoQueryRepository, JpaRepository<TextInfoView, Long> {



}
