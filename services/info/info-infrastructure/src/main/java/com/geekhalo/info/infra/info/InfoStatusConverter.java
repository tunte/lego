package com.geekhalo.info.infra.info;

import com.geekhalo.lego.core.enums.repository.jpa.CommonEnumAttributeConverter;
import javax.persistence.Converter;
import com.geekhalo.info.domain.info.InfoStatus;

@Converter(autoApply = true)
public class InfoStatusConverter 
       extends CommonEnumAttributeConverter<InfoStatus> { 


   public InfoStatusConverter(){ 
       super(InfoStatus.values());
   }


}