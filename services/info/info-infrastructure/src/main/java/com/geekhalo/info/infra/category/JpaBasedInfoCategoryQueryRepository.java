package com.geekhalo.info.infra.category;


import org.springframework.data.jpa.repository.JpaRepository;
import com.geekhalo.info.domain.category.InfoCategoryQueryRepository;
import com.geekhalo.info.domain.category.InfoCategoryView;
import org.springframework.stereotype.Repository;

import java.lang.Long;
import java.util.List;

@Repository("infoCategoryQueryRepository")
public interface JpaBasedInfoCategoryQueryRepository
        extends InfoCategoryQueryRepository,
        JpaRepository<InfoCategoryView, Long> {

}
