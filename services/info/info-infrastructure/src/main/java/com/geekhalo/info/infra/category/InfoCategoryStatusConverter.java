package com.geekhalo.info.infra.category;

import com.geekhalo.lego.core.enums.repository.jpa.CommonEnumAttributeConverter;
import javax.persistence.Converter;
import com.geekhalo.info.domain.category.InfoCategoryStatus;

@Converter(autoApply = true)
public class InfoCategoryStatusConverter 
       extends CommonEnumAttributeConverter<InfoCategoryStatus> { 


   public InfoCategoryStatusConverter(){ 
       super(InfoCategoryStatus.values());
   }


}