package com.geekhalo.info.infra.categorystatis;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.geekhalo.info.domain.categorystatis.CountOfInfoByCategoryCommandRepository;
import com.geekhalo.info.domain.categorystatis.CountOfInfoByCategory;
import java.lang.Long;
import java.util.Optional;

@Repository("countOfInfoByCategoryCommandRepository")
public interface JpaBasedCountOfInfoByCategoryCommandRepository
        extends CountOfInfoByCategoryCommandRepository,
        JpaRepository<CountOfInfoByCategory, Long> {
    @Override
    default CountOfInfoByCategory sync(CountOfInfoByCategory agg){
        return save(agg);
    }



}
