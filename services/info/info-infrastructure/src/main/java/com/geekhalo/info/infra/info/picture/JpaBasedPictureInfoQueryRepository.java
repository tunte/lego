package com.geekhalo.info.infra.info.picture;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.geekhalo.info.domain.info.picture.PictureInfoQueryRepository;
import com.geekhalo.info.domain.info.picture.PictureInfoView;
import java.lang.Long;

@Repository("pictureInfoQueryRepository")
public interface JpaBasedPictureInfoQueryRepository extends PictureInfoQueryRepository, JpaRepository<PictureInfoView, Long> {



}
