package com.geekhalo.info.infra.info.video;


import org.springframework.data.jpa.repository.JpaRepository;
import com.geekhalo.info.domain.info.video.VideoInfoCommandRepository;
import com.geekhalo.info.domain.info.video.VideoInfo;
import java.lang.Long;

public interface JpaBasedVideoInfoCommandRepository extends VideoInfoCommandRepository,JpaRepository<VideoInfo, Long> {
    @Override
    default VideoInfo sync(VideoInfo agg){
        return save(agg);
    }


}
