package com.geekhalo.info.infra.info.video;


import org.springframework.data.jpa.repository.JpaRepository;
import com.geekhalo.info.domain.info.video.VideoInfoQueryRepository;
import com.geekhalo.info.domain.info.video.VideoInfoView;
import java.lang.Long;

public interface JpaBasedVideoInfoQueryRepository extends VideoInfoQueryRepository, JpaRepository<VideoInfoView, Long> {



}
