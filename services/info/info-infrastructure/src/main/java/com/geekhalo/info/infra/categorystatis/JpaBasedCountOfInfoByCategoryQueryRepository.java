package com.geekhalo.info.infra.categorystatis;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.geekhalo.info.domain.categorystatis.CountOfInfoByCategoryQueryRepository;
import com.geekhalo.info.domain.categorystatis.CountOfInfoByCategoryView;
import java.lang.Long;

@Repository("countOfInfoByCategoryQueryRepository")
public interface JpaBasedCountOfInfoByCategoryQueryRepository
        extends CountOfInfoByCategoryQueryRepository,
        JpaRepository<CountOfInfoByCategoryView, Long> {

    CountOfInfoByCategoryView getByCategoryId(Long categoryId);

}
