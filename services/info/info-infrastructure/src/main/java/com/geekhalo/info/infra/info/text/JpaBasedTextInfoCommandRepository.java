package com.geekhalo.info.infra.info.text;


import org.springframework.data.jpa.repository.JpaRepository;
import com.geekhalo.info.domain.info.text.TextInfoCommandRepository;
import com.geekhalo.info.domain.info.text.TextInfo;
import org.springframework.stereotype.Repository;

import java.lang.Long;

public interface JpaBasedTextInfoCommandRepository extends TextInfoCommandRepository,JpaRepository<TextInfo, Long> {
    @Override
    default TextInfo sync(TextInfo agg){
        return save(agg);
    }


}
