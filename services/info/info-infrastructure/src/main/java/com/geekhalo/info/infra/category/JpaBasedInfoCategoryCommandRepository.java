package com.geekhalo.info.infra.category;


import org.springframework.data.jpa.repository.JpaRepository;
import com.geekhalo.info.domain.category.InfoCategoryCommandRepository;
import com.geekhalo.info.domain.category.InfoCategory;
import org.springframework.stereotype.Repository;

import java.lang.Long;

@Repository("infoCategoryCommandRepository")
public interface JpaBasedInfoCategoryCommandRepository extends InfoCategoryCommandRepository,JpaRepository<InfoCategory, Long> {
    @Override
    default InfoCategory sync(InfoCategory agg){
        return save(agg);
    }


}
