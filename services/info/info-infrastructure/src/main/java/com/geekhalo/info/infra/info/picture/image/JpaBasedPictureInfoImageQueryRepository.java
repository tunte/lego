package com.geekhalo.info.infra.info.picture.image;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.geekhalo.info.domain.info.picture.image.PictureInfoImageQueryRepository;
import com.geekhalo.info.domain.info.picture.image.PictureInfoImageView;
import java.lang.Long;

@Repository("pictureInfoImageQueryRepository")
public interface JpaBasedPictureInfoImageQueryRepository extends PictureInfoImageQueryRepository, JpaRepository<PictureInfoImageView, Long> {



}
