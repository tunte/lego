package com.geekhalo.info.infra.info.picture;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.geekhalo.info.domain.info.picture.PictureInfoCommandRepository;
import com.geekhalo.info.domain.info.picture.PictureInfo;
import java.lang.Long;

@Repository("pictureInfoCommandRepository")
public interface JpaBasedPictureInfoCommandRepository extends PictureInfoCommandRepository,JpaRepository<PictureInfo, Long> {
    @Override
    default PictureInfo sync(PictureInfo agg){
        return save(agg);
    }


}
