package ${package}.${rootArtifactId}.config;

import com.geekhalo.lego.core.command.EnableCommandService;
import com.geekhalo.lego.core.query.EnableQueryService;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableCommandService(basePackages = "com.geekhalo.${rootArtifactId}.app")
@EnableQueryService(basePackages = "com.geekhalo.${rootArtifactId}.app")
public class ApplicationServiceConfiguration {
}
